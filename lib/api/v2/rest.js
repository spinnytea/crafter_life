'use strict';
//var _ = require('lodash');

var express = require('express');
exports.endpoint = express();

var items = require('./items');
exports.endpoint.get('/items/count', function(req, res) {
  items.count(req.query).then(res.json.bind(res));
});
exports.endpoint.get('/items/query', function(req, res) {
  items.query(req.query).then(res.json.bind(res));
});
