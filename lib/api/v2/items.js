'use strict';

exports.database = require('../../database');
exports.count = count;
exports.query = query;

function count(params) {
  return exports.database.count('v2/items', transformQueryParams(params));
}
function query(params) {
  return exports.database.query('v2/items', transformQueryParams(params)).then(function(list) {
    if(params.name && (!params.nameType || params.nameType === 'fuzzy')) {
      return textSearch(list, params.name);
    } else {
      return list;
    }
  });
}

function transformQueryParams(params) {
  var ret = {};
  if(!params) return ret;

  if(params.hasRecipe) {
    if(!ret.$or) ret.$or = [];
    ret.$or.push({ recipe: { $exists: true } });
  }

  if(params.hasGather) {
    if(!ret.$or) ret.$or = [];
    ret.$or.push({ gather: { $exists: true } });
  }

  if(params.lodestoneIds) {
    if(typeof params.lodestoneIds === 'string')
      params.lodestoneIds = [ params.lodestoneIds ];
    ret.lodestoneId = { $in: params.lodestoneIds };
  }

  if(params.name && params.nameType === 'regex') {
    ret.name = { $regex: new RegExp(params.name, 'i') };
  }

  if(params.ids) {
    if(typeof params.ids === 'string')
      params.ids = [ params.ids ];
    ret._id = { $in: params.ids };
  }

  if(params.names) {
    if(typeof params.names === 'string')
      params.names = [ params.names ];
    ret.name = { $in: params.names };
  }

  return ret;
}


var FuzzySearch = require('fuzzysearch-js');
var levenshteinFS = require('fuzzysearch-js/js/modules/LevenshteinFS');
var indexOfFS = require('fuzzysearch-js/js/modules/IndexOfFS');
var sift3FS = require('fuzzysearch-js/js/modules/Sift3FS');
//var wordCountFS = require('fuzzysearch-js/js/modules/WordCountFS');
function textSearch(items, term) {
  // XXX I have no idea what any of this configuration is for
  // it's copy-pasta from the example
  var fuzzySearch = new FuzzySearch(items, {'minimumScore': 300, 'termPath': 'name', 'caseSensitive': false});
  fuzzySearch.addModule(levenshteinFS({'maxDistanceTolerance': 3, 'factor': 3}));
  fuzzySearch.addModule(indexOfFS({'minTermLength': 3, 'maxIterations': 500, 'factor': 3}));
  fuzzySearch.addModule(sift3FS({'maxDistanceTolerance': 3, 'factor': 1}));
  //fuzzySearch.addModule(wordCountFS({'maxWordTolerance': 3, 'factor': 1}));

  return fuzzySearch.search(term).map(function(r) {
    r.value.$rank = r.score;
    return r.value;
  });
}
