'use strict';
var _ = require('lodash');
var Promise = require('bluebird'); // jshint ignore:line
var readFile = Promise.promisify(require('fs').readFile);
var database = require('../database');

exports.process = function(status) {
  if(status) status.done = false;
  // the v2 database is one table
  //   items
  return Promise.all([
    readFile('./lib/database/eightylegs/lodestone_item.json', {encoding: 'utf8'}),
    readFile('./lib/database/eightylegs/lodestone_npc.json', {encoding: 'utf8'}),
    readFile('./lib/database/eightylegs/lodestone_recipe.json', {encoding: 'utf8'}),
    readFile('./lib/database/eightylegs/consolegames_location.json', {encoding: 'utf8'})
  ]).then(function (files) {
    files = files.map(JSON.parse);
    var elItems = files[0];
    var elNpcs = _.values(files[1]).filter(function(npc) { return npc.type === 'npc'; });
    var elEnemies = _.values(files[1]).filter(function(npc) { return npc.type === 'enemy'; });
    var elRecipes = files[2];
    var cgCather = files[3];

    //
    // now we have all of the data
    // we still only care about craftable items, and the items those recipes require
    //

    // first only use the items that reference a recipe
    var items = _.filter(elItems, function(elItem) { return elItem.craft; });
    items = _.indexBy(items, 'id');

    // now, add in all the dependencies of all the recipes
    _.forEach(elRecipes, function(elRecipe) {
      _.forEach(elRecipe.recipe, function(r, id) {
        items[id] = elItems[id];
      });
    });


    //
    // prep the raw data
    // combine the data
    //

    items = _.indexBy(_.map(items, function(elItem) {
      var item = {
        lodestoneId: elItem.id,
        name: elItem.name
      };

      // the recipes are stored in the item object
      // (it's a one-to-many relationship, but they links are stored with the one)
      if(elItem.craft) {
        var jobs = _.map(elItem.craft, function(craft) {
          return {
            job: craft.pc,
            level: craft.level
          };
        });

        // all recipes have the same materials
        var elRecipe = elRecipes[_.keys(elItem.craft)[0]];
        var materials = _.map(elRecipe.recipe, function(r, id) {
          return {
            item: id,
            count: r.count
          };
        });

        item.recipe = {
          jobs: jobs,
          materials: materials,
          yield: elRecipe.size
        };
      }

      var gather = cgCather.filter(function(g) { return item.name === g.name; }).map(function(g) {
        return {
          level: g.level,
          job: g.class,
          area: g.region,
          name: g.type,
          coord: { x: g.x, y: g.y }
        };
      });
      if(gather.length) {
        item.gather = gather;
      }

      return item;
    }), 'lodestoneId');


    elEnemies = elEnemies.filter(function(e) { return e.drop; });
    elEnemies.forEach(function(elEnemy) {
      var gs = _.map(elEnemy.spawn, function(spawn, area) {
        if(spawn.cond) return undefined; // don't show conditional spawns
        return {
          minLevel: spawn.level.low,
          maxLevel: spawn.level.high,
          area: area,
          name: elEnemy.name,
          coord: { x: -1, y: -1 } // XXX no coords
        };
      }).filter(_.identity);

      _.keys(elEnemy.drop).forEach(function(itemId) {
        var item = items[itemId];
        if(item) {
          if(!item.hunt) {
            item.hunt = [];
          }
          Array.prototype.push.apply(item.hunt, gs);
        }
      });
    });


    elNpcs = elNpcs.filter(function(n) { return n.sells; });
    elNpcs.forEach(function(elNpc) {
      _.forEach(elNpc.sells, function(obj, itemId) {
        var item = items[itemId];
        if(item) {
          var value = -1;
          var elItem = elItems[itemId];
          if(elItem && elItem.value && _.isNumber(elItem.value.nq)) {
            value = elItem.value.nq;
          }

          item.shop = (item.shop || []);
          item.value = { buy: obj.price, sell: value };
          item.shop.push({
            area: elNpc.region.name,
            name: elNpc.name,
            coord: { x: elNpc.x, y: elNpc.y }
          });
        }
      });
    });


    //
    // finalize the output
    //

    items = _.values(items);

    items.forEach(function(item) {
      // somehow hunt had an empty list; maybe all the hunting locations were conditional
      if(item.hunt && !item.hunt.length)
        delete item.hunt;
    });

    if(status) {
      status.finished = 0;
      status.failed = 0;
      status.total = items.length;
    }

    return database.drop('v2/items').then(function() {
      return Promise.each(items, function(i) {
        return new Promise(function(resolve, reject) {
          database._getDb('v2/items').insert(i, function(err, doc) {
            if(err) {
              if(status) status.failed++;
              reject(err);
            } else {
              if(status) status.finished++;
              resolve(doc);
            }
          });
        });
      });
    });
  }).then(database.compactDatafile).then(function() {
    if(status) status.done = true;
    console.log('done');
  }, function(e) { console.log(e); });
};
