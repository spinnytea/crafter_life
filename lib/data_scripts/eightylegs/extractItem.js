#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var fs = require('fs');

var inpf = process.argv.slice(2, -1);
var outf = process.argv[process.argv.length-1];

var data = [];
inpf.forEach(function (file) {
  JSON.parse(fs.readFileSync(file)).forEach(function (item) {
    if(item.result) {
      data.push(JSON.parse(item.result));
    }
    else {
      console.log('Bad item', item.url);
    }
  });
});

var count = 0;
var items = {};
_.sortBy(data, function (item) { return item.id; }).forEach(function (item) {
  items[item.id] = item;
  count++;
});

console.log(count + ' records');
fs.writeFileSync(outf, JSON.stringify(items, null, 2));