'use strict';

var regex = {
  comma: /,/,
  coord: /\(\s*x(\d+)\s*,\s*y(\d+)\s*\)/,
  btn: /mature\s+tree|lush\s+vegetation/i,
  min: /mineral\s+deposit|rocky\s+outcrop/i,
  primary: /mature\s+tree|mineral\s+deposit/i,
  offhand: /lush\s+vegetation\s+patch|rocky\s+outcrop/i,
  hidden: /\s+\(hidden\)\s*/i
};

var EightyApp = function() { 
  this.processDocument = function(html, url, headers, status, $) { 
    var app = this; 
    var $html = app.parseHtml(html, $);

    // Data is contained within a single table
    var coord, items = [];
    $html.find('table.wikitable tr:has(td)').each(function () {
      var cols = $(this).find('td');
      coord = $(cols[3]).text().trim().match(regex.coord);
      var type = $(cols[1]).text().trim();
      $(cols[4]).text().split(regex.comma).forEach(function (name) {
        items.push({
          name: name.trim().replace(regex.hidden, ''),
          type: type,
          hidden: !!name.match(regex.hidden),
          level: parseInt($(cols[0]).text().trim()),
          region: $(cols[2]).text().trim(),
          'class': (type.match(regex.btn))
            ? 'BTN'
            : (type.match(regex.min) ? 'MIN' : 'UNK'),
          weapon: (type.match(regex.primary)) 
            ? 'primary' 
            : (type.match(regex.offhand) ? 'offhand' : 'unknown'),
          x: parseInt(coord[1]),
          y: parseInt(coord[2])
        });
      });
    });

    return items;
  };

  this.parseLinks = function() {}; 
};

module.exports = function(EightyAppBase) { 
  EightyApp.prototype = new EightyAppBase(); 
  return new EightyApp(); 
};