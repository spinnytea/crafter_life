'use strict';
var _ = require('lodash');

var regex = {
  furl: /^https?\:\/\/(.*)$/,
  slash: /\//,
  comma: /,/g,
  space: /\s+/,
  number: /\d+/,
  range: /(\d+)(?:\s*-\s*(\d+))?|\?\?/,
  bool: /true|yes/i,
  time: /[d|h|m|s]/,
  nq: /nq/i,
  hq: /hq/i,
  value: /sells\s+for\s+(\d+)\s+gil/i,
  npc: /^selling\s+npc/i,
  quest: /related\s+quests/i,
  craft: /^crafting\s+log/i,
  gather: /^gathering\s+log/i,
  recipe: /([^\(]+)(?:\((\d+)\))?/,
  total: /^total\s+crafted/i,
  drop: /^dropped\sitem/i,
  spawn: /^spawn\s+location/i,
  location: /^location/i,
  shop: /^shop\s+items/i,
  candr: /crafting\s+\&\s+repairs/i,
  repair: /repair\s+level/i,
  meld: /materia\s+melding/i,
  coord: /x\:\s+(\d+)\s+y\:\s+(\d+)/i,
  enemy: /^enemies/i,
  npct: /NPC/,
  params: {
    magic: /magic\s+damage/i,
    physical: /physical\s+damage/i,
    auto: /auto-attack/i,
    delay: /delay/i,
    recast: /recast/i,
  },
  props: {
    convert: /convertible\:\s+(yes|no)/i,
    project: /projectable\:\s+(yes|no)/i,
    desynth: /desynthesizable\:\s+(yes|no)/i,
    dyeable: /dyeable\:\s+(yes|no)/i,
    crest: /crest-worthy\:\s+(yes|no)/i
  },
  bonus: /bonuses/i,
  statv: /([+|-]?\d+)(\%?)/,
  setbn: /^(\d+)\s+equipped\s*\:\s*(.*)$/i,
  slots: {
    primary: /Primary|Grimoire|Arm/,
    offhand: /Secondary|Shield|Two.*Arm/,
    head: /Head/,
    body: /Body/,
    hands: /Hands/,
    waist: /Waist/,
    legs: /Legs/,
    feet: /Feet/,
    neck: /Necklace/,
    ears: /Earrings/,
    wrist: /Bracelets/,
    ring: /Ring/,
    crystal: /Soul\s+Crystal/,
  },
  nohead: /cannot\s+equip\s+gear\s+to\s+head/i,
  stats: {
    hp: /HP/,
    mp: /MP/,
    tp: /TP/,
    cp: /CP/,
    gp: /GP/,
    str: /strength/i,
    dex: /dexterity/i,
    vit: /vitality/i,
    int: /intelligence/i,
    mnd: /mind/i,
    pty: /piety/i,
    acc: /accuracy/i,
    chr: /critical\s+hit\s+rate/i,
    det: /determination/i,
    def: /defense/i,
    pry: /parry/i,
    mdf: /magic\s+defense/i,
    apr: /attack\s+power/i,
    sks: /skill\s+speed/i,
    amp: /attack\s+magic\s+potency/i,
    hmp: /healing\s+magic\s+potency/i,
    sps: /spell\s+speed/i,
    crf: /craftsmanship/i,
    ctl: /control/i,
    gth: /gathering/i,
    prc: /perception/i
  },
  resist: {
    fire: /fire\s+resistance/i,
    ice: /ice\s+resistance/i,
    wind: /wind\s+resistance/i,
    earth: /earth\s+resistance/i,
    lightning: /lightning\s+resistance/i,
    water: /water\s+resistance/i,
    slash: /slashing\s+resistance/i,
    pierce: /piercing\s+resistance/i,
    blunt: /blunt\s+resistance/i,
    slow: /slow\s+resistance/i,
    silence: /silence\s+resistance/i,
    blind: /blind\s+resistance/i,
    poison: /poison\s+resistance/i,
    stun: /stun\s+resistance/i,
    sleep: /sleep\s+resistance/i,
    bind: /bind\s+resistance/i,
    heavy: /heavy\s+resistance/i,
    amnesia: /amnesia\s+resistance/i,
    bleed: /bleed\s+resistance/i,
    dropsy: /dropsy\s+resistance/i,
    paralysis: /paralysis\s+resistance/i,
    petrification: /petrification\s+resistance/i,
  },
  groups: {
    ALL: /All Classes/,
    DOW: /Disciple\s+of\s+War/,
    DOM: /Disciple\s+of\s+Magic/,
    DOH: /Disciple\s+of\s+Hand/,
    DOL: /Disciple\s+of\s+Land/
  },
  war: {
    ARC: /Archer|ARC/,
    BRD: /Bard|BRD/,
    GLA: /Gladiator|GLA/,
    PLD: /Paladin|PLD/,
    LNC: /Lancer|LNC/,
    DRG: /Dragoon|DRG/,
    MRD: /Marauder|MRD/,
    WAR: /Warrior|WAR/,
    PGL: /Pugilist|PGL/,
    MNK: /Monk|MNK/,
    ROG: /Rogue|ROG/,
    NIN: /Ninja|NIN/,
    MCH: /Machinist|MCH/,
    DRK: /Dark\s+Knight|DRK/
  },
  magic: {
    CNJ: /Conjurer|CNJ/,
    WHM: /White\s+Mage|WHM/,
    THM: /Thaumaturge|THM/,
    BLM: /Black\s+Mage|BLM/,
    ACN: /Arcanist|ACN/,
    SMN: /Summoner|SMN/,
    SCH: /Scholar|SCH/,
    AST: /Astrologian|AST/
  },
  hand: {
    ALC: /Alchemist|ALC/,
    ARM: /Armorer|ARM/,
    BSM: /Blacksmith|BSM/,
    CRP: /Carpenter|CRP/,
    CUL: /Culinarian|CUL/,
    GSM: /Goldsmith|GSM/,
    LTW: /Leatherworker|LTW/,
    WVR: /Weaver|WVR/
  },
  land: {
    BTN: /Harvesting|Logging|Botanist|BTN/,
    MIN: /Mining|Quarrying|Miner|MIN/,
    FSH: /Fisher|FSH/
  }
};

function splitUrl(url) {
  url = '' + url;
  var match;
  if((match = url.match(regex.furl))) url = match[1];
  if(url[url.length-1] === '/') url = url.substr(0, url.length-1);
  return url.split(regex.slash);
}

function extractId(url) {
  var params = splitUrl(url);
  return params[params.length-1];
}

function extractRef(url) {
  var params = splitUrl(url);
  return params[params.length-2];
}

function extractValue ($, item, key) {
  /* jshint validthis: true */
  var match = $(this).text().trim().match(regex.value);
  if(match) {
    if(!item.value) item.value = {};
    item.value[key] = parseInt(match[1]);
  }
  return item;
}

function extractKeyFromGroup(text, group) {
  var res;
  return _.some(group, function (pattern, key) {
    if(text.match(pattern)) {
      res = key;
      return true;
    }
    return false;
  }) ? res : null;
}

function parseNumber(num) {
  if(num.match(regex.time)) return num;
  return parseFloat(num);
}

function parseStatValue(stat) {
  var match = stat.match(regex.statv);
  return {
    value: parseFloat(match[1]),
    unit: (match[2]) ? 'percent' : 'number'
  };
}

function extractStatGroup(stat, group) {
  var key, value;
  return (_.some(group, function (pattern, name) {
    if(stat.match(pattern)) {
      key = name;
      value = parseStatValue(stat);
      return true;
    }
    return false;
  })) ? [key, value] : null;
}

function extractStat(stat) { return extractStatGroup(stat, regex.stats); }

function extractResist(stat) { return extractStatGroup(stat, regex.resist); }

var EightyApp = function() { 
  this.processDocument = function(html, url, headers, status, $) { 
    var app = this; 
    var $html = app.parseHtml(html, $);
    var details = $html.find('div.item_detail_box');
    var temp;
    var item = {};

    // All of the data is inside of this div
    $html = $html.find('div.db_cnts');

    // Basic properties
    item.id   = extractId(url);
    $html.find('h2.item_name').each(function () {
      item.name = $(this).text().trim();
    });
    $html.find('div.db_detail_name span').each(function () {
      item.name = $(this).text().trim();
    });
    
    // Item type
    item.type = extractRef(url);

    // Item classes
    $html.find('div.class_ok').each(function () {
      if(!item['class']) item['class'] = {};
      var cls = $(this).text().trim();
      if(!_.some(regex.groups, function (pattern, name) {
        if(cls.match(pattern)) {
          item['class'][name] = true;
          return true;
        }
        return false;
      })) {
        _.forEach(cls.split(regex.space), function (cls) {
          function updateClass(pattern, name) {
            if(cls.match(pattern)) item['class'][name] = true;
          }
          _.forEach(regex.war,   updateClass);
          _.forEach(regex.magic, updateClass);
          _.forEach(regex.hand,  updateClass);
          _.forEach(regex.land,  updateClass);
        });
      }
    });

    // Item slots
    $html.find('div.item_name_area div').each(function () {
      if(!item.slots) item.slots = {};
      var slot = $(this).clone().find('>*').remove().end().text().trim();
      _.forEach(regex.slots, function (pattern, name) {
        if(slot.match(pattern)) item.slots[name] = true;
      });
    });
    // One-off case for cowls and other head-limiting objects
    if(details.text().trim().match(regex.nohead)) {
      if(!item.slots) item.slots = {};
      item.slots.head = true;
    }

    // Item levels
    $html.find('div.eorzeadb_tooltip_pb3').each(function () {
      if(!item.level) item.level = {};
      item.level.item = parseInt($(this).text().match(regex.number));
    });
    $html.find('div.gear_level').each(function () {
      if(!item.level) item.level = {};
      item.level.gear = parseInt($(this).text().match(regex.number));
    });

    // Item params
    var pname = $html.find('div.parameter_name');
    var pnq = $html.find('div.sys_nq_element div.parameter');
    var phq = $html.find('div.sys_hq_element div.parameter');
    if(pname.length > 0 && pnq.length === pname.length 
      && phq.length === pname.length) {
      var handleParam = function (pattern, name) {
        if($(pname[i]).text().match(pattern)) {
          if(!item.param) item.param = {};
          item.param[name] = {
            nq: parseNumber($(pnq[i]).text().trim()),
            hq: parseNumber($(phq[i]).text().trim())
          };
        }
      };
      var i; for(i = 0; i < pname.length; ++i) {
        _.some(regex.params, handleParam);
      }
    }

    // Item stats
    function statQuality(qual) {
      /* jshint validthis: true */
      if($(this).text().match(regex.bonus)) {
        $(this).parent().find('ul.basic_bonus li').each(function () {
          var stat = $(this).text().trim();
          temp = extractStat(stat);
          if(temp) {
            if(!item.stats) item.stats = {};
            if(!item.stats[qual]) {
              item.stats[qual] = {};
            }
            item.stats[qual][temp[0]] = temp[1];
          }
          temp = extractResist(stat);
          if(temp) {
            if(!item.resist) item.resist = {};
            if(!item.resist[qual]) {
              item.resist[qual] = {};
            }
            item.resist[qual][temp[0]] = temp[1];
          }
        });
        $(this).parent().find('li.set_bonus').each(function () {
          if(!item.set) item.set = {
            name: details.find('div.series').text().trim()
          };
          var match = $(this).text().trim().match(regex.setbn);
          if(match) {
            temp = extractStat(match[2]);
            if(temp) {
              item.set.count = parseInt(match[1]);
              if(!item.set.stats) item.set.stats = {};
              item.set.stats[temp[0]] = temp[1];
            }
            temp = extractResist(match[2]);
            if(temp) {
              item.set.count = parseInt(match[1]);
              if(!item.set.resist) item.set.resist = {};
              item.set.resist[temp[0]] = temp[1];
            }
          }
        });
      }
    }
    details.find('div.sys_nq_element h3').each(function () {
      statQuality.call(this, 'nq');
    });
    details.find('div.sys_hq_element h3').each(function () {
      statQuality.call(this, 'hq');
    });

    // Materia
    var materia = details.find('ul.materia li');
    if(materia.length > 0) item.materia = { slots: materia.length };

    details.find('h3').each(function () {
      if($(this).text().trim().match(regex.candr)) {
        $(this).nextAll('ul').find('li').each(function () {
          var cols = $(this).find('div');
          if(cols.length === 2) {
            var label = $(cols[0]).text().trim();
            var value = $(cols[1]).text().trim();
            // Repair
            if(label.match(regex.repair)) {
              if(!item.repair) item.repair = {};
              item.repair['class'] = extractKeyFromGroup(value, regex.hand);
              value = value.match(regex.number);
              item.repair.level = parseInt(value[0]);
            }
            // Melding
            else if(label.match(regex.meld)) {
              if(!item.meld) item.meld = {};
              temp = extractKeyFromGroup(value, regex.hand);
              if(temp) item.meld['class'] = temp;
              else temp = extractKeyFromGroup(value, regex.land);
              if(temp) item.meld['class'] = temp;
              value = value.match(regex.number);
              item.meld.level = parseInt(value[0]);
            }
          }
          // Properties
          else {
            var text = $(this).text().trim();
            _.forEach(regex.props, function (pattern, name) {
              temp = text.match(pattern);
              if(temp) {
                item[name] = !!(temp[1].match(regex.bool));
              }
            });
          }
        });
      }
    });

    // Unique
    details.find('span.rare').each(function () {
      item.unique = ($(this).text().trim().length > 0);
    });

    // Tradable
    details.find('span.ex_bind').each(function () {
      item.trade = ($(this).text().trim().length === 0);
    });

    // Item value
    details.find('span.sys_nq_element').each(function () {
      item = extractValue.call(this, $, item, 'nq');
    });
    details.find('span.sys_hq_element').each(function () {
      item = extractValue.call(this, $, item, 'hq');
    });

    // Recipe components
    $html.find('div.recipe_detail table td:not([class])').each(function () {
      if(!item.recipe) item.recipe = {};
      var iref = $(this).find('a.db_popup');
      var recipe = iref.text().trim().match(regex.recipe);
      item.recipe[extractId(iref.attr('href'))] = {
        ref: extractRef(iref.attr('href')),
        name: recipe[1].trim(),
        count: (recipe[2]) ? parseInt(recipe[2]) : 1
      };
    });

    // Recipe size
    $html.find('div.recipe_detail dt').each(function () {
      if($(this).text().trim().match(regex.total)) {
        item.size = parseInt($(this).next().text());
      }
    });
    
    $html.find('h3').each(function () {
      // Vendors
      var text = $(this).text().trim();
      if(text.match(regex.npc)) {
        $(this).parent().find('tbody tr').each(function () {
          if(!item.vendor) item.vendor = {};
          var cols = $(this).find('td');
          var vendor = $(cols[0]).find('a');
          item.vendor[extractId(vendor.attr('href'))] = {
            ref: extractRef(vendor.attr('href')),
            name: vendor.text().trim(),
            region: {
              name: $(cols[1]).text().trim()
            }
          };
        });
      }
      // Quests
      else if(text.match(regex.quest)) {
        $(this).parent().find('tbody tr').each(function () {
          if(!item.quest) item.quest = {};
          var cols = $(this).find('td');
          var quest = $(cols[0]).find('a.db_popup');
          item.quest[extractId(quest.attr('href'))] = {
            ref: extractRef(quest.attr('href')),
            name: quest.text().trim(),
            level: parseInt($(cols[2]).text().trim()),
            region: {
              name: $(cols[1]).text().trim()
            }
          };
        });
      }
      // Crafting
      else if(text.match(regex.craft)) {
        $(this).parent().find('tbody tr').each(function () {
          if(!item.craft) item.craft = {};
          var cols = $(this).find('td');
          var type = $(cols[0]).find('a.db_popup');
          item.craft[extractId(type.attr('href'))] = {
            ref: extractRef(type.attr('href')),
            pc: extractKeyFromGroup(type.text().trim(), regex.hand),
            level: parseInt($(cols[1]).text().trim())
          };
        });
      }
      // Gathering
      else if(text.match(regex.gather)) {
        $(this).parent().find('tbody tr').each(function () {
          if(!item.gather) item.gather = {};
          var cols = $(this).find('td');
          var type = $(cols[0]).find('a.db_popup');
          item.gather[extractId(type.attr('href'))] = {
            ref: extractRef(type.attr('href')),
            pc: extractKeyFromGroup(type.text().trim(), regex.land),
            level: parseInt($(cols[1]).text().trim())
          };
        });
      }
      // Dropped items
      else if(text.match(regex.drop)) {
        $(this).parent().find('tbody tr').each(function () {
          if(!item.drop) item.drop = {};
          var cols = $(this).find('td');
          var iref = $(cols[0]).find('a.db_popup');
          item.drop[extractId(iref.attr('href'))] = {
            ref: extractRef(iref.attr('href')),
            name: iref.text().trim()
          };
        });
      }
    });

    $html.find('div.db_detail_wrap tr').each(function () {
      // Spawn location
      if($(this).text().trim().match(regex.spawn)) {
        $(this).closest('table').find('li').each(function () {
          if(!item.spawn) item.spawn = {};
          var lvl = $(this).clone().find('>*').remove().end().text().trim()
            .match(regex.range);
          var loc = $(this).find('a');
          item.spawn[loc.text().trim()] = {
            cond: ($(this).find('p').length > 0),
            level: {
              low:  parseInt(lvl[1]) || -1,
              high: parseInt(lvl[2]) || parseInt(lvl[1]) || -1
            }
          };
        });
      }
      // NPC Location
      else if($(this).text().match(regex.location)) {
        $(this).closest('table').find('ul.place').each(function () {
          if(!item.coord) item.region = {};
          var root = $(this).children().first();
          item.region.name = 
            root.clone().find('>*').remove().end().text().trim();
          var pos = $(this).find('ul li').text().match(regex.coord);
          item.x = parseInt(pos[1]);
          item.y = parseInt(pos[2]);
        });
      }
      // Shop items
      else if($(this).text().match(regex.shop)) {
        $(this).closest('table').find('table tbody tr').each(function () {
          if(!item.sells) item.sells = {};
          var cols = $(this).find('td');
          var sell = $(cols[0]).find('a.db_popup');
          item.sells[extractId(sell.attr('href'))] = {
            ref: extractRef(sell.attr('href')),
            name:  sell.text().trim(),
            price: parseInt($(cols[1]).text().replace(regex.comma, ''))
          };
        });
      }
    });

    return item;
  };

  this.parseLinks = function() {}; 
};

module.exports = function(EightyAppBase) { 
  EightyApp.prototype = new EightyAppBase(); 
  return new EightyApp(); 
};