'use strict';
var EightyApp = function() { 
  this.processDocument = function(html, url, headers, status, $) { 
    var app = this; 
    var $html = app.parseHtml(html, $);

    // All of the data is inside of this div
    $html = $html.find('div.db_cnts');

    // Scrape item links
    var links = [];
    $html.find('a.db_popup').each(function () {
      links.push(app.makeLink(url, $(this).attr('href')));
    });
    return links;
  };

  this.parseLinks = function() {}; 
};

module.exports = function(EightyAppBase) { 
  EightyApp.prototype = new EightyAppBase(); 
  return new EightyApp(); 
};