#!/usr/bin/env node
'use strict';
var path = require('path');
var EightyLegs = require('./eightylegs');

var appn = path.basename(process.argv[2]);

var api = new EightyLegs();
api.deleteApp(appn).then(function () {
  console.log('Deleted application \'' + appn + '\'');
}, console.error);