#!/usr/bin/env node
'use strict';
var path = require('path');
var EightyLegs = require('./eightylegs');

var list = path.basename(process.argv[2], '.json');

var api = new EightyLegs();
api.deleteList(list).then(function () {
  console.log('Deleted list \'' + list + '\'');
}, console.error);