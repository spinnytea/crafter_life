#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var fs = require('fs');

var inpf = process.argv.slice(2);

var list = [];
inpf.forEach(function (file) {
  JSON.parse(fs.readFileSync(file)).forEach(function (item) {
    if(_.isUndefined(item.result)) {
      console.error(item);
      list.push(item.url);
    }
  });
});

console.error('Errors: ' + list.length);
console.log(JSON.stringify(list, null, 2));
