#!/usr/bin/env node
'use strict';
var fs = require('fs');
var path = require('path');
var EightyLegs = require('./eightylegs');

var file = process.argv[2];
var list = process.argv[3] || path.basename(file, path.extname(file));

var data = JSON.parse(fs.readFileSync(file).toString());

var api = new EightyLegs();
api.updateList(list, data).then(function () {
   console.log('Updated list \'' + list + '\'');
}, console.error);