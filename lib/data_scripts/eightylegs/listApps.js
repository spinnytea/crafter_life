#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var EightyLegs = require('./eightylegs');
var api = new EightyLegs();
api.getApps().then(function (apps) {
  _.forEach(apps, function ($, app) {
    console.log(app);
  });
}, console.error);