#!/usr/bin/env node
'use strict';
var path = require('path');
var EightyLegs = require('./eightylegs');
if(process.argv.length < 3) {
  console.error(
    'Usage: ' + path.relative(process.cwd(), process.argv[1]) + ' [script]'
  );
  process.exit(1);
}
EightyLegs.buildApp(path.join(process.cwd(), process.argv[2]))
  .then(function (buf) {
    console.log(buf.toString());
  });