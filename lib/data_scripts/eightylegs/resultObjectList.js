#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var fs = require('fs');

var inpf = process.argv[2];
var outf = process.argv[3];

var list = [];

JSON.parse(fs.readFileSync(inpf)).forEach(function (page) {
  JSON.parse(page.result).forEach(function (obj) {
    list.push(obj);
  });
});

function objectSort (obj) {
  return (obj.id) ? obj.id : obj.name;
}

fs.writeFileSync(outf, JSON.stringify(_.sortBy(list, objectSort), null, 2));