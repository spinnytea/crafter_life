#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var fs = require('fs');

var inpf = process.argv[2];
var outf = process.argv[3];

var list = [];

JSON.parse(fs.readFileSync(inpf)).forEach(function (page) {
  JSON.parse(page.result).forEach(function (link) {
    list.push(link);
  });
});

fs.writeFileSync(outf, JSON.stringify(_.sortBy(list), null, 2));