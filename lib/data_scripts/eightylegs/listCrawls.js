#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var EightyLegs = require('./eightylegs');
var api = new EightyLegs();

var maxDigits = ('' + EightyLegs.maxUrls).length;

api.getCrawls().then(function (crawls) {
  crawls = crawls.slice(0, Math.min(11, crawls.length));
  _.forEach(crawls, function (crawl) {
    if(crawl.status !== 'CANCELED') {
      console.log(
        '[' + _.padRight(crawl.status, 9) + '] ' + 
        '(' + _.padLeft(crawl.urls_crawled, maxDigits) + ') ' + 
        crawl.name
      );
    }
  });
}, console.error);
