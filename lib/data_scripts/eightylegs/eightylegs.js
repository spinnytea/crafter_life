'use strict';
var _ = require('lodash');
var request = require('request');
var browserify = require('browserify');
var uglify = require('uglify-js');
var dereq = require('derequire');

function joinUrl () {
  return _.flattenDeep(arguments).join('/');
}

function EightyLegs(token) {
  this.token = token || process.env.EIGHTYLEGS_API_TOKEN;
}

EightyLegs.api = { 
  base: 'api.80legs.com/v2',
  apps: 'apps',
  lists: 'urllists',
  crawls: 'crawls',
  results: 'results'
};

EightyLegs.maxUrls = 10000;

EightyLegs.prototype.url = function buildUrl () {
  return joinUrl(
    'https://' + this.token + ':@' + EightyLegs.api.base,
    arguments
  );
};

EightyLegs.prototype.getApps = function getApps () {
  var endpoint = this.url(EightyLegs.api.apps);
  return new Promise(function (resolve, reject) {
    request.get({
      url: endpoint
    }, function (err, response, body) {
      if(err) return reject(err);
      var apps = {};
      JSON.parse(body).forEach(function (app) {
        apps[app.name] = true;
      });
      resolve(apps);
    });
  });
};

// Code passed to 80legs needs to be lintable
function cleanupCode (code) {
  var formatted = '';

  // Remove require functions
  code = dereq(code);

  // Compress the output
  formatted += uglify.minify(code, {
    fromString: true,
    mangle: true,
    compress: true,
    // output: {
    //   beautify: true,
    //   indent_level: 2,
    //   width: 72,
    //   quote_style: 1,
    //   bracketize: true
    // }
  }).code;

  return formatted;
}

EightyLegs.buildApp = function buildApp (path) {
  return new Promise(function (resolve, reject) {
    browserify(path, { standalone: 'EightyApp' })
      .bundle(function (err, buf) {
        if(err) reject(err);
        resolve(new Buffer(cleanupCode(buf.toString())));
      });
  });
};

EightyLegs.prototype.createApp = function createApp (name, path) {
  var endpoint = this.url(EightyLegs.api.apps, name);
  return EightyLegs.buildApp(path).then(function (code) {
    return new Promise(function (resolve, reject) {
      request.put({
        url: endpoint,
        headers: { 'Content-Type': 'application/octet-stream' },
        body: code
      }, function (err, response, body) {
        if(err) return reject(err);
        if(response.statusCode !== 204) return reject(response.statusMessage);
        resolve(body);
      });
    });
  });
};

EightyLegs.prototype.deleteApp = function deleteApp (name) {
  var endpoint = this.url(EightyLegs.api.apps, name);
  return new Promise(function (resolve, reject) {
    request.del({
      url: endpoint
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 204) return reject(response.statusMessage);
      resolve(body);
    });
  });
};

EightyLegs.prototype.updateApp = function updateApp (name, path) {
  return this.getApps()
    .then(function (apps) {
      if(apps[name]) return this.deleteApp(name);
    }.bind(this))
    .then(function () {
      return this.createApp(name, path);
    }.bind(this));
};

EightyLegs.prototype.getLists = function getLists () {
  var endpoint = this.url(EightyLegs.api.lists);
  return new Promise(function (resolve, reject) {
    request.get({
      url: endpoint
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 200) return reject(response.statusMessage);
      var lists = {};
      JSON.parse(body).forEach(function (list) {
        lists[list.name] = true;
      });
      resolve(lists);
    });
  });
};

EightyLegs.prototype.createList = function createList (name, urls) {
  var endpoint = this.url(EightyLegs.api.lists, name);
  return new Promise(function (resolve, reject) {
    request.put({
      url: endpoint,
      headers: { 'Content-Type': 'application/octet-stream' },
      body: new Buffer(JSON.stringify(urls))
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 204) return reject(response.statusMessage);
      resolve(body);
    });
  });
};

EightyLegs.prototype.deleteList = function deleteList (name) {
  var endpoint = this.url(EightyLegs.api.lists, name);
  return new Promise(function (resolve, reject) {
    request.del({
      url: endpoint
    }, function (err, response, body) {
      if(err) reject(err);
      if(response.statusCode !== 204) return reject(response.statusMessage);
      resolve(body);
    });
  });
};

EightyLegs.prototype.updateList = function updateList (name, urls) {
  return this.getLists()
    .then(function (lists) {
      if(lists[name]) return this.deleteList(name);
    }.bind(this))
    .then(function() {
      return this.createList(name, urls);
    }.bind(this));
};

EightyLegs.prototype.getCrawls = function getCrawls () {
  var endpoint = this.url(EightyLegs.api.crawls);
  return new Promise(function (resolve, reject) {
    request.get({
      url: endpoint
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 200) return reject(response.statusMessage);
      resolve(JSON.parse(body));
    });
  });
};

EightyLegs.prototype.cancelCrawl = function cancelCrawl (name) {
  var endpoint = this.url(EightyLegs.api.crawls, name);
  return new Promise(function (resolve, reject) {
    request.del({
      url: endpoint
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 204) return reject(response.statusMessage);
      resolve(body);
    });
  });
};

EightyLegs.prototype.createCrawl = 
function createCrawl (name, app, list, depth) {
  var endpoint = this.url(EightyLegs.api.crawls, name);
  return new Promise(function (resolve, reject) {
    request.put({
      url: endpoint,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        app: app,
        urllist: list,
        max_depth: depth,
        max_urls: EightyLegs.maxUrls
      })
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 204) return reject(response.statusMessage);
      resolve(body);
    });
  });
};

EightyLegs.prototype.getResults = function getResults (name) {
  var endpoint = this.url(EightyLegs.api.results, name);
  return new Promise(function (resolve, reject) {
    request.get({
      url: endpoint,
      headers: { 'Content-Type': 'application/json' },
    }, function (err, response, body) {
      if(err) return reject(err);
      if(response.statusCode !== 200) return reject(response.statusMessage);
      resolve(Promise.all(JSON.parse(body).map(function (output) {
        return new Promise(function (iresolve, ireject) {
          request.get({
            url: output,
          }, function (ierr, iresponse, ibody) {
            if(ierr) return ireject(ierr);
            if(iresponse.statusCode !== 200) 
              return ireject(iresponse.statusMessage);
            iresolve(JSON.parse(ibody));
          });
        });
      })));
    });
  });
};

module.exports = EightyLegs;