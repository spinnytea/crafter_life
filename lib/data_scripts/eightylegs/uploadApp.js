#!/usr/bin/env node
'use strict';
var path = require('path');
var EightyLegs = require('./eightylegs');

var file = process.argv[2];
var appn = process.argv[3] || path.basename(file);

var api = new EightyLegs();
api.updateApp(appn, file).then(function () {
  console.log('Updated application \'' + appn + '\'');
}, console.error);