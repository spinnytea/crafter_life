#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var EightyLegs = require('./eightylegs');
var api = new EightyLegs();
api.getLists().then(function (lists) {
  _.forEach(lists, function ($, list) {
    console.log(list);
  });
}, console.error);