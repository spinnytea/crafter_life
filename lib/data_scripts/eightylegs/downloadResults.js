#!/usr/bin/env node
'use strict';
var _ = require('lodash');
var fs = require('fs');
var EightyLegs = require('./eightylegs');

var crln = process.argv[2];
var file = process.argv[3];

var api = new EightyLegs();
api.getResults(crln).then(function (results) {
  results = _.flattenDeep(results);
  console.log('Got ' + results.length + ' results');
  fs.writeFileSync(file, JSON.stringify(results, null, 2));
}, console.error);