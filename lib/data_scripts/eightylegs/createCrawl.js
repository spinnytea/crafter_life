#!/usr/bin/env node
'use strict';
var path = require('path');
var EightyLegs = require('./eightylegs');

var list = process.argv[2];
list = path.basename(list, path.extname(list));
var appn = path.basename(process.argv[3]);
var dept = parseInt(process.argv[4]);
var cnam = list + '_' + Date.now();

var api = new EightyLegs();
api.createCrawl(cnam, appn, list, dept).then(function () {
  console.log(cnam);
}, console.error);