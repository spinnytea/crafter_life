#!/usr/bin/env node
'use strict';
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var EightyLegs = require('./eightylegs');

var inpf = process.argv[2];
var outf = process.argv[3];

var data = _.shuffle(JSON.parse(fs.readFileSync(inpf)));

var lists = [];
var list = [];
var count = 0;
data.forEach(function (item) {
  list.push(item);
  count++;
  if(count > (EightyLegs.maxUrls-2)) {
    lists.push(list);
    list = [];
    count = 0;
  }
});
if(list.length > 0) lists.push(list);

if(lists.length === 1) {
  console.log('Bin 1 (' + lists[0].length + ')');
  fs.writeFileSync(outf, JSON.stringify(lists[0], null, 2));
}
else {
  var root = path.dirname(outf);
  var ext  = path.extname(outf);
  var base = path.basename(outf, ext);
  count = 1;
  lists.forEach(function (list) {
    console.log('Bin ' + count + ' (' + list.length + ')');
    fs.writeFileSync(
      path.join(root, base + '_' + count + ext),
      JSON.stringify(list, null, 2)
    );
    count++;
  });
}