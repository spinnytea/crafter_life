'use strict';
// this is an adapter on nedb
var _ = require('lodash');
var Datastore = require('nedb');

var openTables = {};

exports._getDb = getDb;
function getDb(table) {
  var db = openTables[table];
  if(!db) {
    db = openTables[table] = new Datastore({ filename: './lib/database/'+table+'.db' });
    db.loadDatabase();
  }
  return db;
}

exports.compactDatafile = function() {
  _.forEach(openTables, function(db, table) {
    console.log('compacting: ' + table);
    db.persistence.compactDatafile();
  });
};

exports.drop = function(table) {
  return new Promise(function(resolve, reject) {
    getDb(table).remove({}, { multi: true }, function(err, count) {
      if(err) reject(err);
      resolve(count);
    });
  });
};

exports.count = function(table, queryParams) {
  return new Promise(function(resolve, reject) {
    getDb(table).count(queryParams, function(err, count) {
      if(err) reject(err);
      resolve(count);
    });
  });
};

exports.query = function(table, params) {
  return new Promise(function(resolve, reject) {
    getDb(table).find(params, function(err, list) {
      if(err) reject(err);
      resolve(list);
    });
  });
};

exports.get = function(table, key) {
  return new Promise(function(resolve, reject) {
    getDb(table).findOne(key, function(err, obj) {
      if(err) reject(err);
      resolve(obj);
    });
  });
};

exports.update = function(table, obj, keys) {
  return new Promise(function(resolve, reject) {
    var final = {};
    flatten(final, obj, '');

    var key = {};
    keys.forEach(function(k) {
      key[k] = final[k];
    });

    var db = getDb(table);
    db.update(key, { $set: final }, { upsert: true }, function(err) {
      if(err) reject(err);
      db.findOne(key, function(err, obj) {
        if(err) reject(err);
        resolve(obj);
      });
    });
  });
};

function flatten(final, curr, prefix) {
  var key;
  for(key in curr) {
    var flatKey = prefix + '.' + key;
    var value = curr[key];

    if(typeof value === 'object') {
      flatten(final, value, flatKey);
    } else {
      final[flatKey.substr(1)] = value;
    }
  }
}