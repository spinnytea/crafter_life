'use strict';
module.exports = angular.module('crafter_life', [
  require('./common/commonModule').name,
  require('./home/homeModule').name,
  require('./life/lifeModule').name,
  require('./page/pageModule').name,
  require('./recipes/recipeModule').name,
  require('./api').name,
  'ngAnimate',
  'ngRoute'
]);
module.exports.config([
  '$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/home/home.html',
        controller: 'crafter_life.home.controller as home',
      })
      .when('/recipes', {
        templateUrl: '/recipes/recipeSearch.html',
        controller: 'crafter_life.recipes.controller',
      })
      .when('/life/list', {
        templateUrl: '/life/recipes/userRecipeList.html',
        controller: 'crafter_life.life.recipeList.controller',
      })
      .when('/life/craft', {
        templateUrl: '/life/adventure/craftList.html',
        controller: 'crafter_life.life.craftList.controller',
      })
      .when('/settings', {
        templateUrl: '/page/settings.html',
        controller: 'crafter_life.settings.controller as settings',
      })
      .when('/about', {
        templateUrl: '/page/about.html',
      })
      .otherwise({
        templateUrl: '/oops.html',
      });
  }
]);
