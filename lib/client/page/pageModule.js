'use strict';

module.exports = angular.module('crafter_life.page', [
  require('./pageController').name,
  require('./settingsController').name
]);
