'use strict';
var _ = require('lodash');
var cakeCore = require('../common/CakeCore');

module.exports = angular.module('crafter_life.settings.controller', []);
module.exports.controller('crafter_life.settings.controller', [
  '$scope', '$q', 'crafter_life.api', 'crafter_life.life.data.service',
  function($scope, $q, api, userDataService) {
    // TODO handle upload error
    // TODO can we access $scope.page without $scope?

    var that = this; // because 'this' changes inside of functions

    //
    // theme settings
    //

    this.themes = [ $scope.page.theme ]; // seed the list so the user doesn't notice
    api.themes().then(function(list) { that.themes = list; }); // fill in the full list once we have it

    this.saveTheme = function() {
      userDataService.set('theme', $scope.page.theme);
    };

    //
    // saved data
    //
    this.canUpload = !!(window.File && window.FileReader && window.FileList && window.Blob);

    function dataFromCurrent() {
      return {
        version: 'helium', // version 2
        goals: userDataService.craft.current(),
        input: userDataService.craft.userInput()
      };
    }

    function updateCurrentStats() {
      that.current.goalCount = userDataService.craft.current().length;
    }

    this.current = {
      save: function() {
        savedCraftList.push({
          name: cakeCore.random(),
          date: new Date().toISOString(),
          data: dataFromCurrent()
        });
        userDataService.craft.saved(savedCraftList);
      },
      upload: function() {
        uploadData($q).then(function(data) {
          userDataService.craft.update(data.goals);
          userDataService.craft.userInput(data.input);
          updateCurrentStats();
        }, function(err) {
          console.log(err);
        });
      },
      download: function() {
        downloadData('save', dataFromCurrent());
      },
      delete: function() {
        userDataService.craft.update([]);
        userDataService.craft.userInput({});
        updateCurrentStats();
      }
    };

    this.list = {
      update: function() {
        userDataService.craft.saved(savedCraftList);
      },
      load: function(item) {
        userDataService.craft.update(item.data.goals);
        userDataService.craft.userInput(item.data.input);
        updateCurrentStats();
      },
      copy: function(item) {
        var clone = _.clone(item);
        clone.date = new Date().toISOString();
        savedCraftList.push(clone);
        userDataService.craft.saved(savedCraftList);
      },
      upload: function(item) {
        uploadData($q).then(function(data) {
          item.date = new Date().toISOString();
          item.data = data;
          userDataService.craft.saved(savedCraftList);
        }, function(err) {
          console.log(err);
        });
      },
      download: function(item) {
        downloadData(item.name, item.data);
      },
      delete: function(item) {
        _.remove(savedCraftList, item);
        userDataService.craft.saved(savedCraftList);
      }
    };

    var savedCraftList = this.savedCraftList = userDataService.craft.saved();
    updateCurrentStats();

    //
    // data clearing
    //

    this.clearSavedAdventures = function() {
      userDataService.craft.saved([]);
      savedCraftList.splice(0);
    };

    this.clearAllSavedData = function() {
      userDataService.clearAll();

      // reset page theme
      $scope.page.theme = undefined;
      $scope.page.setupDefaultTheme();
      that.saveTheme();

      // reset saved data cache
      updateCurrentStats();
      savedCraftList.splice(0);
    };

  }
]);

function uploadData($q) {
  var deferred = $q.defer(),
    e = document.createEvent('MouseEvents'),
    a = document.createElement('input');

  function readSingleFile(evt) {
    //Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0];

    if (f) {
      var r = new FileReader();
      r.onload = function(e) {
        var contents = e.target.result;
        try {
          var parsed = JSON.parse(contents);
          if(angular.isObject(parsed) && parsed.version === 'helium' && angular.isArray(parsed.goals) && angular.isObject(parsed.input)) {
            deferred.resolve(parsed);
          } else {
            deferred.reject('old data');
          }
        } catch(e) {
          deferred.reject('not json');
        }
      };
      r.readAsText(f);
    } else {
      deferred.reject('could not read');
    }
  }

  a.type = 'file';
  a.addEventListener('change', readSingleFile, false);
  e.initMouseEvent('click', true, false, window,
    0, 0, 0, 0, 0, false, false, false, false, 0, null);
  a.dispatchEvent(e);

  return deferred.promise;
}

function downloadData(name, data) {
  data = JSON.stringify(data, null, 2);
  var filename = name+'.json';

  var blob = new Blob([data], {type: 'text/json'}),
    e = document.createEvent('MouseEvents'),
    a = document.createElement('a');

  a.download = filename;
  a.href = window.URL.createObjectURL(blob);
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':');
  e.initMouseEvent('click', true, false, window,
    0, 0, 0, 0, 0, false, false, false, false, 0, null);
  a.dispatchEvent(e);
}
