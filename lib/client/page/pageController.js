'use strict';

module.exports = angular.module('crafter_life.page.controller', []);
module.exports.controller('crafter_life.page.controller', [
  '$location', 'crafter_life.life.data.service',
  function($location, userDataService) {
    this.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };

    this.theme = userDataService.get('theme');
    this.setupDefaultTheme = function() {
      if(!this.theme) this.theme = 'yeti';
    };
    this.setupDefaultTheme();
  }
]);
