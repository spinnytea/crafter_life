'use strict';
var _ = require('lodash');

module.exports = angular.module('crafter_life.api', []);
module.exports.factory('crafter_life.api', [
  '$q', '$http',
  function($q, $http) {
    var api = {};

    api.themes = function() {
      if(api.themes.cached) {
        return $q.resolve(api.themes.cached);
      } else {
        return $http.get('/rest/themes').then(function(success) {
          api.themes.cached = success.data;
          return success.data;
        });
      }
    };

    api.jobs = _.indexBy(jobs, 'abbreviation');

    api.items = {};
    api.items.count = function(hasRecipe) {
      var params = {};
      if(hasRecipe) params.hasRecipe = true;
      return $http.get('/rest/v2/items/count', { params: params })
        .then(function(success) { return success.data; });
    };
    api.items.query = function(params) {
      return $http.get('/rest/v2/items/query', { params: params })
        .then(function(success) {
          var list = success.data;
          list.forEach(function(i) {
            if(i.gather) {
              i.gather.forEach(function(g) {
                g.coord.display = coordDisplay(g.coord);
              });
            }
            if(i.shop) {
              i.shop.forEach(function(s) {
                s.coord.display = coordDisplay(s.coord);
              });
            }
          });
          return list;
        });
    };

    return api;
  }
]);

var jobs = [
  { type: 'war', name: 'Gladiator', abbreviation: 'GLA' },
  { type: 'war', name: 'Pugilist', abbreviation: 'PGL' },
  { type: 'war', name: 'Marauder', abbreviation: 'MRD' },
  { type: 'war', name: 'Lancer', abbreviation: 'LNC' },
  { type: 'war', name: 'Archer', abbreviation: 'ARC' },
  { type: 'war', name: 'Rouge', abbreviation: 'ROG' },

  { type: 'magic', name: 'Conjurer', abbreviation: 'CNJ' },
  { type: 'magic', name: 'Thaumaturge', abbreviation: 'THM' },
  { type: 'magic', name: 'Arcanist', abbreviation: 'ACN' },

  { type: 'hand', name: 'Carpenter', abbreviation: 'CRP' },
  { type: 'hand', name: 'Blacksmith', abbreviation: 'BSM' },
  { type: 'hand', name: 'Armorer', abbreviation: 'ARM' },
  { type: 'hand', name: 'Goldsmith', abbreviation: 'GSM' },
  { type: 'hand', name: 'Leatherworker', abbreviation: 'LTW' },
  { type: 'hand', name: 'Weaver', abbreviation: 'WVR' },
  { type: 'hand', name: 'Alchemist', abbreviation: 'ALC' },
  { type: 'hand', name: 'Culinarian', abbreviation: 'CUL' },

  { type: 'land', name: 'Miner', abbreviation: 'MIN' },
  { type: 'land', name: 'Botanist', abbreviation: 'BTN' },
  { type: 'land', name: 'Fisher', abbreviation: 'FSH' }
];

function coordDisplay(obj) {
  return '(x'+obj.x+',y'+obj.y+')';
}
