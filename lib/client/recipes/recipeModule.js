'use strict';

module.exports = angular.module('crafter_life.recipes', [
  require('./recipeSearchController').name
]);
