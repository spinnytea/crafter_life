'use strict';
var _ = require('lodash');
//
// search for items that have recipes
//

module.exports = angular.module('crafter_life.recipes.controller', []);
module.exports.controller('crafter_life.recipes.controller', [
  '$scope', 'crafter_life.api', 'crafter_life.common.search', 'crafter_life.life.data.service',
  function($scope, api, Search, userDataService) {

    $scope.itemFormModel = {
      name: userDataService.get('search.name'),
      nameType: userDataService.get('search.nameType') || 'fuzzy'
    };

    $scope.addToItemList = function(item) {
      return userDataService.list.add(item);
    };
    $scope.getItemListCount = function(item) {
      return userDataService.list.count(item);
    };


    $scope.search = new Search(function(params) {
      return api.items.query(params).then(function(list) {
        list.forEach(function(item) {
          if(item.gather) {
            var gather = {};
            item.gather.forEach(function(ig) {
              var sg = gather[ig.job];
              if(!sg) {
                sg = gather[ig.job] = {
                  job: ig.job,
                  areas: {},
                  minLevel: ig.level,
                  maxLevel: ig.level
                };
              }
              sg.areas[ig.area] = true;
              sg.minLevel = Math.min(sg.minLevel, ig.level);
              sg.maxLevel = Math.max(sg.maxLevel, ig.level);
            });
            gather = _.values(gather);
            gather.forEach(function(sg) {
              sg.areas = _.keys(sg.areas).join(', ');
            });
            gather = _.sortBy(gather, 'job');
            item.gather = gather;
          }
        });
        return list;
      });
    }, 'name');
    $scope.$on('destroy', $scope.$watch('itemFormModel.name', doSearch));
    $scope.$on('destroy', $scope.$watch('itemFormModel.nameType', doSearch));
    function doSearch() {
      var params = {
        hasRecipe: true,
        hasGather: true,
        name: $scope.itemFormModel.name,
        nameType: $scope.itemFormModel.nameType,
      };

      userDataService.set('search.name', params.name);
      userDataService.set('search.nameType', params.nameType);

      if(params.name && params.name.replace(/\\\w|[.*]/g, '').length > 2) {
        $scope.search.go(params);
      } else {
        $scope.search.reset();
      }
    }
  }
]);
