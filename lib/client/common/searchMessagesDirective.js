'use strict';

module.exports = angular.module('crafter_life.common.search.messages', []);
module.exports.directive('searchMessages', [
  function SearchMessagesDirective() {
    return {
      scope: { search: '=searchMessages' },
      templateUrl: '/common/searchModel.html',
      controller: [
        '$scope',
        SearchMessagesController
      ]
    };

    function SearchMessagesController($scope) {
      var status = $scope.search.status;

      $scope.showSearchingMessage = function() {
        return status.isSearching && !status.hasSearched;
      };

      $scope.showErrorMessage = function() {
        return !status.isSearching && status.hasError;
      };

      $scope.showFirstSearchMessage = function() {
        return !status.isSearching && !status.hasError && !status.hasSearched;
      };

      $scope.showNoResultsMessage = function() {
        return !status.isSearching && !status.hasError && status.hasSearched &&
          ($scope.search.results.list.length === 0);
      };
    }
  }
]);