'use strict';

module.exports = angular.module('crafter_life.common.jobLevel.directive', []);
module.exports.directive('jobLevel', [
  function JobLevelDirective() {
    return {
      scope: { model: '=jobLevel' },
      templateUrl: '/common/jobLevel.html',
    };
  }
]);