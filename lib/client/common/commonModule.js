'use strict';

module.exports = angular.module('crafter_life.common', [
  require('./jobLevelDirective').name,
  require('./search').name,
  require('./searchMessagesDirective').name
]);
