'use strict';
var _ = require('lodash');

module.exports = angular.module('crafter_life.common.search', []);
module.exports.factory('crafter_life.common.search', [
  function() {
    function Search(searchFn, matchProp) {
      this.status = {
        hasSearched: false,
        isSearching: false,
        hasError: false
      };
      this.results = {
        list: []
      };

      this.searchFn = searchFn;
      this.matchProp = matchProp;
    }

    Search.prototype.go = function(queryParams) {
      this.status.isSearching = true;
      this.searchFn(queryParams).then(this.setSearchResults.bind(this), (function() {
        this.status.hasSearched = true;
        this.status.isSearching = false;
        this.status.hasError = true;
      }).bind(this));
    };

    Search.prototype.setSearchResults = function(list) {

      // ngAnimate tracks objects; we can't switch out the object
      // so use the object from the last search when we can
      var oldResults = _.indexBy(this.results.list, this.matchProp);
      var self = this;
      this.results.list = list.map(function(obj) {
        var old = oldResults[obj[self.matchProp]];
        if(old) {
          // TODO allow for custom special keys
          old.$rank = obj.$rank;
          return old;
        }
        return obj;
      });

      this.status.hasSearched = true;
      this.status.isSearching = false;
      this.status.hasError = false;
    };

    Search.prototype.reset = function() {
      this.results.list = [];
      this.status.hasSearched = false;
      this.status.isSearching = false;
      this.status.hasError = false;
    };

    Search.prototype.hasResults = function() {
      return this.results.list.length;
    };

    return Search;
  }
]);