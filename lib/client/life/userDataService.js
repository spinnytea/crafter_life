'use strict';

module.exports = angular.module('crafter_life.life.data.service', []);
module.exports.factory('crafter_life.life.data.service', [
  'localStorageService',
  function(localStorageService) {
    var service = {};

    // simple pass-through for simple things
    service.set = localStorageService.set;
    service.get = localStorageService.get;
    service.clearAll = function() {
      localStorageService.clearAll();
      userRecipeList = {};
    };


    var userRecipeList = localStorageService.get('life.recipe.list') || {};
    service.list = {};
    service.list.add = function(i) {
      var item = userRecipeList[i.lodestoneId] = (userRecipeList[i.lodestoneId] || {});
      item.lodestoneId = i.lodestoneId;
      item.count = ((item.count || 0)+1);
      return localStorageService.set('life.recipe.list', userRecipeList);
    };
    service.list.remove = function(i) {
      delete userRecipeList[i.lodestoneId];
      return localStorageService.set('life.recipe.list', userRecipeList);
    };
    service.list.clear = function() {
      userRecipeList = {};
      return localStorageService.set('life.recipe.list', userRecipeList);
    };
    service.list.count = function(i) {
      var item = userRecipeList[i.lodestoneId];
      if(!item) return null;
      return item.count;
    };
    service.list.values = function() {
      return userRecipeList;
    };
    service.list.save = function() {
      return localStorageService.set('life.recipe.list', userRecipeList);
    };


    service.craft = {};
    // lift.craft.list is a list of item IDs
    // lift.craft.userInput is the find/buy values for the current adventure
    // TODO collapse current (update and current don't need to be separate functions)
    service.craft.update = function(list) {
      return localStorageService.set('life.craft.list', list);
    };
    service.craft.current = function() {
      return localStorageService.get('life.craft.list') || [];
    };
    service.craft.userInput = function(userInput) {
      if(userInput) {
        return localStorageService.set('life.craft.userInput', userInput);
      } else {
        return localStorageService.get('life.craft.userInput') || {};
      }
    };

    service.craft.saved = function(list) {
      if(list) {
        return localStorageService.set('life.craft.saved', list);
      } else {
        return localStorageService.get('life.craft.saved') || [];
      }
    };


    return service;
  }
]);
