'use strict';

var regions = [
  {
    name: 'The Black Shroud',
    color: 'darkkhaki',
    areas: [
      'New Gridania', 'Old Gridania',
      'North Shroud', 'Central Shroud', 'East Shroud', 'The Lavender Beds', 'Lotus Stand',
      'South Shroud', 'The Roost', 'The Striking Tree', 'Thornmarch'
    ]
  },
  {
    name: 'La Noscea',
    color: 'crimson',
    areas: [
      'Limsa Lominsa Lower Decks', 'Limsa Lominsa Upper Decks',
      'Command Room', 'Eastern La Noscea', 'Lower La Noscea', 'Middle La Noscea', 'Mist',
      'Outer La Noscea', 'Seal Rock', 'The Navel', 'The Whorleater', 'The Wolves\' Den',
      'Upper La Noscea', 'Western La Noscea', 'Wolves\' Den Pier',
    ]
  },
  {
    name: 'Thanalan',
    color: 'olivedrab',
    areas: [
      'Ul\'dah - Steps of Nald', 'Ul\'dah - Steps of Thal',
      'Bowl of Embers', 'Central Thanalan', 'Eastern Thanalan', 'Heart of the Sworn', 'Hourglass',
      'Northern Thanalan', 'Southern Thanalan', 'The Eighteenth Floor', 'The Fragrant Chamber', 'The Goblet',
      'The Gold Saucer', 'Western Thanalan',
    ]
  },
];

var AREAS = {};

var order = 0;
regions.forEach(function(region) {
  AREAS[region.name] = {
    region: region.name,
    order: (++order),
    color: region.color,
  };

  region.areas.forEach(function(name) {
    AREAS[name] = {
      region: region.name,
      order: (++order),
      color: region.color,
    };
  });
});

var unknown = 'Region Unknown (to Crafter Life)';
AREAS[unknown] = {
  region: unknown,
  order: (++order),
};

exports.region = function(area) {
  var a = AREAS[area] || AREAS[unknown];
  return a.region;
};

exports.color = function(area) {
  var a = AREAS[area];
  return (a && a.color) || 'lightgrey';
};

exports.order = function(area) {
  var a = AREAS[area];
  return (a && a.order) || area;
};
