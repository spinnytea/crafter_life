'use strict';
var _ = require('lodash');
var cakeCore = require('../../common/CakeCore');

module.exports = angular.module('crafter_life.life.recipeList.controller', []);
module.exports.controller('crafter_life.life.recipeList.controller', [
  '$scope', 'crafter_life.api', 'crafter_life.life.data.service',
  function($scope, api, userDataService) {
    $scope.randomJumbotronNoun = cakeCore.random();

    $scope.itemSelections = userDataService.list.values();
    $scope.itemCache = [];

    function redoItemCounts() {
      $scope.itemSelectionCount = _.size($scope.itemSelections);
      $scope.itemSelectionSum = _.sum($scope.itemSelections, 'count');
    }

    $scope.deleteFromList = function(item) {
      userDataService.list.remove(item);
      redoItemCounts();
    };
    $scope.saveList = function() {
      userDataService.list.save();
      redoItemCounts();
    };

    $scope.clearList = function() {
      userDataService.list.clear();
      $scope.itemSelections = userDataService.list.values();
      redoItemCounts();
    };

    $scope.newAdventure = function() {
      userDataService.craft.userInput({});
      userDataService.craft.update(_.values($scope.itemSelections));
    };

    $scope.setCraftList = function(selection) {
      if(selection) {
        userDataService.craft.update([selection]);
      } else {
        userDataService.craft.update(_.values($scope.itemSelections));
      }
      //$location.path('/life/craft');
    };

    redoItemCounts();
    if($scope.itemSelectionCount) {
      api.items.query({ lodestoneIds: _.keys($scope.itemSelections) }).then(function(list) {
        $scope.itemCache = _.indexBy(list, 'lodestoneId');
      });
    }
  }
]);
