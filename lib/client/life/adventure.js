'use strict';
var _ = require('lodash');
var standardItems = require('./standardItems');
var areas = require('./areas');
// this file contains the fancy logic for building, prepping, expanding, organizing, figure-outing, ...
// this is the core logic behind the Crafting Adventure

// request all items layer by layer
// minimizes the number of queries
// @param allItems: all the items we need for the supplied list (name -> item map)
// @param list: the list of items we are going to craft (the starting point)
// @param query: the query({names:[...]}) will return a list of items that match the names
exports.fetchAllItems = function(allItems, list, query) {
  return query({ lodestoneIds: list }).then(function(curr) {
    var next = [];

    curr.forEach(function(item) {
      if(item.recipe) {
        allItems[item.lodestoneId] = item;
        Array.prototype.push.apply(next, _.pluck(item.recipe.materials, 'item'));
      } else {
        allItems[item.lodestoneId] = item;
      }
    });

    // don't query for items we already have
    next = next.filter(function(n) { return !allItems[n]; });

    // recurse if we have more to look for
    if(next.length)
      return exports.fetchAllItems(allItems, next, query);

    // the final product of the query is ignored
    // XXX should we return Promise.resolve(allItems)?
  });
};


// @param allItems: all the items we need for the supplied list (name -> item map)
// @param list: the list of items we are going to craft (the starting point)
// @return a topologically sorted list (the elements of list will be near the end)
exports.setupCraftingDAG = function(allItems, list) {
  var dag = {
    map: {},
  };

  function traverse(currListItem, order) {
    var id = currListItem.lodestoneId;
    var c = dag.map[id];
    if(!c) {
      c = dag.map[id] = {
        id: id,
        name: allItems[id].name,
        order: 0, // depth
        standard: false,
        final: false,
        total: 0,
        remaining: 0,
        userInput: {
          obtained: 0,
          action: undefined // 'craft', 'gather', 'hunt', 'shop'
        }
      };
    }
    c.order = Math.max(c.order, order);

    // if this item as a recipe, then recurse
    if(allItems[id].recipe) {
      c.gatherable = false;
      c.yield = allItems[id].recipe.yield;
      c.userInput.action = 'craft';
      c.minLevel = allItems[id].recipe.jobs.reduce(function(min, j) { return Math.min(min, j.level); }, 1000);
      allItems[id].recipe.materials.forEach(function(m) {
        traverse({
          lodestoneId: m.item
        }, order+1);
      });
    } else {
      c.gatherable = true; // this mean gather
      if(standardItems.isStandardItem(c)) {
        c.standard = true;
        c.userInput.obtained = -1;
      }

      if(allItems[id].gather) {
        c.userInput.action = 'gather';
      } else if(allItems[id].shop) {
        c.userInput.action = 'shop';
      } else if(allItems[id].hunt) {
        c.userInput.action = 'hunt';
      }
    }
  }
  list.forEach(function(currListItem) { traverse(currListItem, 1); });
  list.forEach(function(currListItem) { dag.map[currListItem.lodestoneId].final = true; });

  dag.list = _.values(dag.map);
  dag.list = _.sortBy(dag.list, 'order');

  // create a topological order
  var topologicalOrder = 1;
  var didAThing = true;
  // start with non-craftable items
  dag.list.forEach(function(dagItem) { if(!allItems[dagItem.id].recipe) dagItem.topologicalOrder = topologicalOrder; });
  while(didAThing) {
    didAThing = false;
    topologicalOrder++;
    // as a first pass, mark everything on this level
    dag.list.forEach(function(dagItem) {
      if(!dagItem.hasOwnProperty('topologicalOrder')) {
        var isLeaf = allItems[dagItem.id].recipe.materials.every(function(mat) {
          return dag.map[mat.item].topologicalOrder;
        });
        if(isLeaf) {
          dagItem.topologicalOrder = null;
          didAThing = true;
        }
      }
    });
    // apply the topologicalOrder to the leaves
    dag.list.forEach(function(dagItem) {
      if(dagItem.topologicalOrder === null)
        dagItem.topologicalOrder = topologicalOrder;
    });
  }

  exports.recalculateCounts(allItems, list, dag);

  return dag;
};

exports.recalculateCounts = function(allItems, list, dag) {
  // reset the totals
  // - totals are used for current state
  // - raw_totals are only used for progress
  dag.list.forEach(function(dagItem) { dagItem.total = dagItem.raw_total = 0; });
  // set the seed counts
  list.forEach(function(currListItem) { dag.map[currListItem.lodestoneId].total = dag.map[currListItem.lodestoneId].raw_total = currListItem.count; });
  // do the topological traversal
  dag.list.forEach(function(dagItem) {
    // by this point the current value is the total we need for THIS ITEM
    // so we can use it as a multiplier for its dependencies
    dagItem.remaining = Math.max(0, dagItem.total - dagItem.userInput.obtained);

    if(allItems[dagItem.id].recipe) {
      var material_multiplier = 0;
      var raw_multiplier = 0;
      if(dagItem.userInput.action === 'craft')
        material_multiplier = Math.ceil(dagItem.remaining / dagItem.yield);
      raw_multiplier = Math.ceil(dagItem.raw_total / dagItem.yield);
      allItems[dagItem.id].recipe.materials.forEach(function(m) {
        dag.map[m.item].total += m.count * material_multiplier;
        dag.map[m.item].raw_total += m.count * raw_multiplier;
      });
    }
  });
  // mark all the standard items as finished
  dag.list.forEach(function(dagItem) {
    if(dagItem.standard && dagItem.userInput.obtained === -1) {
      dagItem.userInput.obtained = dagItem.total;
      dagItem.remaining = 0;
    }
  });
};

exports.buildLocations = function(allItems, dag) {
  var locations = {};

  var list = dag.list.filter(function(dagItem) { return dagItem.remaining > 0; });
  list.forEach(function(dagItem) {
    var item = allItems[dagItem.id];

    if(item.gather && dagItem.userInput.action === 'gather') {
      item.gather.forEach(function(itemLoc) {
        var l = locations[itemLoc.area] = (locations[itemLoc.area] || []);
        l.push({
          job: { job: itemLoc.job, level: itemLoc.level },
          dagItem: dagItem,
          item: dagItem.name,
          name: itemLoc.name,
          coord: itemLoc.coord.display
        });
      });
    }

    if(item.hunt && dagItem.userInput.action === 'hunt') {
      item.hunt.forEach(function(itemLoc) {
        var l = locations[itemLoc.area] = (locations[itemLoc.area] || []);
        l.push({
          job: { job: 'MOB', minLevel: itemLoc.minLevel, maxLevel: itemLoc.maxLevel },
          dagItem: dagItem,
          item: dagItem.name,
          name: itemLoc.name,
          coord: itemLoc.coord.display
        });
      });
    }

    if(item.shop && dagItem.userInput.action === 'shop') {
      item.shop.forEach(function(itemLoc) {
        var l = locations[itemLoc.area] = (locations[itemLoc.area] || []);
        l.push({
          job: { job: 'BUY' },
          dagItem: dagItem,
          item: dagItem.name,
          name: itemLoc.name,
          coord: itemLoc.coord.display
        });
      });
    }
  });

  // add all the regions
  Object.keys(locations).forEach(function(area) {
    var region = areas.region(area);
    locations[region] = [];
  });

  return _.map(locations, function(list, area) { return {
    area: area,
    region: areas.region(area),
    order: areas.order(area),
    isRegion: (area === areas.region(area)),
    list: list
  }; });
};

exports.buildLevelRequirements = function(allItems, dag) {
  // we want the list to be sorted
  var levelReqs = {
    'CRP': null, 'BSM': null, 'ARM': null, 'GSM': null,
    'LTW': null, 'WVR': null, 'ALC': null, 'CUL': null,
    'MIN': null, 'BTN': null,
    'MOB': null, 'BUY': null,
  };

  var list = dag.list.filter(function(dagItem) { return dagItem.remaining > 0; });
  list.forEach(function(dagItem) {
    var item = allItems[dagItem.id];
    var itemLoc;

    if(item.recipe && dagItem.userInput.action === 'craft') {
      itemLoc = _.min(item.recipe.jobs, function(itemLoc) { return itemLoc.level; });

      if(!levelReqs[itemLoc.job] || levelReqs[itemLoc.job].level < itemLoc.level)
        levelReqs[itemLoc.job] = { job: itemLoc.job, level: itemLoc.level };
    }

    if(item.gather && dagItem.userInput.action === 'gather') {
      itemLoc = _.min(item.gather, function(itemLoc) { return itemLoc.level; });

      if(!levelReqs[itemLoc.job] || levelReqs[itemLoc.job].level < itemLoc.level)
        levelReqs[itemLoc.job] = { job: itemLoc.job, level: itemLoc.level };
    }

    if(item.hunt && dagItem.userInput.action === 'hunt') {
      itemLoc = _.min(item.hunt, function(itemLoc) { return itemLoc.minLevel; });

      if(!levelReqs.MOB || levelReqs.MOB.level < itemLoc.minLevel)
        levelReqs.MOB = { job: 'MOB', level: itemLoc.minLevel };
    }

    if(item.shop && dagItem.userInput.action === 'shop') {
      if(!levelReqs.BUY)
        levelReqs.BUY = { job: 'BUY', level: 0 };
      levelReqs.BUY.level += dagItem.remaining*item.value.buy;
    }
  });

  return _.values(levelReqs).filter(function(v) { return v; });
};

// build an inverse mapping of recipe materials
exports.buildCraftInto = function(allItems) {
  var craftInto = {};

  _.filter(allItems, function(item) { return item.recipe; }).forEach(function(item) {
    item.recipe.materials.forEach(function(mat) {
      (craftInto[mat.item] = (craftInto[mat.item] || [])).push(item.lodestoneId);
    });
  });

  return craftInto;
};
