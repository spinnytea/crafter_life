'use strict';
// this is where all the user-centric stuff goes

module.exports = angular.module('crafter_life.life', [
  require('./adventure/craftListController').name,
  require('./adventure/selectedItemDirective').name,
  require('./recipes/userRecipeListController').name,
  require('./userDataService').name,
  'LocalStorageModule'
]);
module.exports.config([
  'localStorageServiceProvider',
  function(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('crafter_life');
  }
]);