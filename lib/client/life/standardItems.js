'use strict';

var names = [];

[ 'Earth', 'Fire', 'Ice', 'Lightning', 'Water', 'Wind' ].forEach(function(element) {
  [ 'Shard', 'Crystal', 'Cluster' ].forEach(function(crystal) {
    names.push(element + ' ' + crystal);
  });
});

exports.isStandardItem = function(item) {
  return names.indexOf(item.name) !== -1;
};
