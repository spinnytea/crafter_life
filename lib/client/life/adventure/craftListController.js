'use strict';
var _ = require('lodash');
var adventure = require('../adventure');

module.exports = angular.module('crafter_life.life.craftList.controller', []);
module.exports.controller('crafter_life.life.craftList.controller', [
  '$scope', 'crafter_life.api', 'crafter_life.life.data.service',
  function($scope, api, userDataService) {
    $scope.sectionStandardOpen = false;
    $scope.sectionGatherOpen = true;
    $scope.sectionCraftOpen = true;

    var current = userDataService.craft.current();
    $scope.userInput = userDataService.craft.userInput();
    if(!current.length) {
      $scope.noCurrent = true;
      $scope.hasRecipes = !!_.size(userDataService.list.values());
      return;
    }

    // our authoritative list of all the items we need to collect
    // either through buying, hunting, gathering, or crafting
    var items = $scope.items = {};
    var dag;
    adventure.fetchAllItems(items, _.pluck(current, 'lodestoneId'), api.items.query).then(function() {
      dag = $scope.dag = adventure.setupCraftingDAG(items, current);
      // either save or restore the dagItem input
      dag.list.forEach(function(dagItem) {
        if(dagItem.id in $scope.userInput) {
          dagItem.userInput = $scope.userInput[dagItem.id];
        } else {
           $scope.userInput[dagItem.id] = dagItem.userInput;
        }

        if(dagItem.final && dagItem.order === 1) {
          dagItem.craftSortOrder = '1000' + pad(dagItem.minLevel, 3);
        } else {
          dagItem.craftSortOrder = '0' + pad(dagItem.topologicalOrder, 3) + pad(dagItem.minLevel, 3);
        }
      });

      $scope.craftInto = adventure.buildCraftInto(items);
    });

    // the dag list pretty much only contains all the data we want to respond to
    // so maybe it isn't soo bad?
    $scope.$on('destroy', $scope.$watch('dag.list', function(newList) {
      if(newList) {
        $scope.locations = adventure.buildLocations(items, dag);
        $scope.levelReqs = adventure.buildLevelRequirements(items, dag);
        adventure.recalculateCounts(items, current, dag);
        userDataService.craft.userInput($scope.userInput);

        $scope.gatherProgress = calculateProgress(dag.list.filter(function(dagItem) { return dagItem.gatherable && !dagItem.standard; }));
        $scope.craftProgress = calculateProgress(dag.list.filter(function(dagItem) { return !dagItem.gatherable; }));

        // init the view with fully expanded regions
        if(!$scope.expandedRegions) {
          $scope.expandedRegions = {};
          $scope.locations.forEach(function(loc) {
            $scope.expandedRegions[loc.region] = true;
          });
        }
      }
    }, true));

    function calculateProgress(list) {
      var progress = {};
      progress.total = _.sum(list, 'raw_total');
      progress.remaining = _.sum(list, 'remaining');
      progress.percent = 100 * ( progress.total - progress.remaining ) / progress.total;
      progress.display = Math.floor(progress.percent) + '%';
      return progress;
    }

    //
    //
    //

    $scope.selectedDagItem = null;
    $scope.selectedItem = null;
    $scope.select = function(dagItem) {
      if($scope.selectedDagItem) {
        // clean up the previous selection
        // (only to prep for a new one)
        $scope.selectedDagItem.selected = false;
      }

      if(_.isString(dagItem)) {
        // this is actually the lodestone id
        dagItem = dag.map[dagItem];
      }

      if($scope.selectedDagItem !== dagItem) {
        // make a new selection
        $scope.selectedDagItem = dagItem;
        $scope.selectedItem = $scope.items[dagItem.id];
        dagItem.selected = true;
      } else {
        // clear the previous selection
        $scope.selectedDagItem = null;
        $scope.selectedItem = null;
      }
    };
    $scope.isSelected = function(dagItem) {
      return dagItem === $scope.selectedDagItem;
    };
  }
]);

module.exports.directive('trDagItem', [function() {
  return {
    replace: true,
    scope: {
      items: '=', // only needed for recipes
      selectCallback: '=', // a when we want to select or deselect and item
      isSelectedCallback: '=', // a when we want to select or deselect and item

      dagItem: '=',
      item: '='
    },
    templateUrl: '/life/adventure/dagItem.html',
    controller: ['$scope', function($scope) {
      if($scope.item.gather) {
        $scope.gather = {};
        $scope.item.gather.forEach(function(ig) {
          var sg = $scope.gather[ig.job];
          if(!sg) {
            sg = $scope.gather[ig.job] = {
              job: ig.job,
              areas: {},
              minLevel: ig.level,
              maxLevel: ig.level
            };
          }
          sg.areas[ig.area] = true;
          sg.minLevel = Math.min(sg.minLevel, ig.level);
          sg.maxLevel = Math.max(sg.maxLevel, ig.level);
        });
        $scope.gather = _.values($scope.gather);
        $scope.gather.forEach(function(sg) {
          sg.areas = _.keys(sg.areas).join(', ');
        });
        $scope.gather = _.sortBy($scope.gather, 'job');
      }
      if($scope.item.hunt) {
        $scope.item.hunt.forEach(function(ig) {
          var sg = $scope.hunt;
          if(!sg) {
            sg = $scope.hunt = {
              job: 'MOB',
              areas: {},
              minLevel: ig.minLevel,
              maxLevel: ig.maxLevel
            };
          }
          sg.areas[ig.area] = true;
          sg.minLevel = Math.min(sg.minLevel, ig.minLevel);
          sg.maxLevel = Math.max(sg.maxLevel, ig.maxLevel);
        });

        if($scope.hunt)
          $scope.hunt.areas = _.keys($scope.hunt.areas).join(', ');
      }

      if($scope.item.shop) {
        $scope.shopAreas = $scope.item.shop.reduce(function(ret, s) { ret[s.area] = true; return ret; }, {});
        $scope.shopAreas = _.keys($scope.shopAreas).join(', ');
      }

      if($scope.item.recipe) {
        $scope.materialNameList = _.pluck($scope.item.recipe.materials, 'item')
          .map(function(id) { return $scope.items[id].name; })
          .join(', ');
      }
    }]
  };
}]);

function pad(num, size) {
  var s = '000000000' + num;
  return s.substr(s.length-size);
}
