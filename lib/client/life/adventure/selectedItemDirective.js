'use strict';
var _ = require('lodash');

module.exports = angular.module('crafter_life.life.craftList.selectedItem.directive', []);
module.exports.directive('selectedItem', [function() {
  return {
    replace: true,
    scope: {
      dagItem: '=',
      item: '=',

      items: '=',
      craftInto: '=',
      selectCallback: '='
    },
    templateUrl: '/life/adventure/selectedItem.html',
    controller: [
      '$scope',
      function($scope) {
        $scope.$on('destroy', $scope.$watch('item', function(item) {
          if(item && $scope.craftInto && $scope.items) {
            $scope.craftIntoList = buildLists([], item);
          } else {
            $scope.craftIntoList = undefined;
          }
        }));

        function buildLists(list, item) {
          list.push(item);
          var cis = $scope.craftInto[item.lodestoneId];
          if(!cis) return [list];

          return _.flatten(cis.map(function(ci) {
            return buildLists(list.slice(), $scope.items[ci]);
          }));
        }
      }
    ]
  };
}]);
