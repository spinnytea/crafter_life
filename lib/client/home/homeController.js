'use strict';
var _ = require('lodash');

module.exports = angular.module('crafter_life.home.controller', []);
module.exports.controller('crafter_life.home.controller', [
  'crafter_life.api',
  function(api) {

    this.jobStats = {
      count: _.size(api.jobs),
      handCount: _.values(api.jobs).filter(function(j) { return j.type === 'hand'; }).length,
      landCount: _.values(api.jobs).filter(function(j) { return j.type === 'land'; }).length,
    };

    // load data
    api.items.count(true).then((function(count) {
      this.recipe_count = count;
    }).bind(this));
    api.items.count().then((function(count) {
      this.item_count = count;
    }).bind(this));
  }
]);
