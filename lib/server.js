'use strict';
var path = require('path');
var fs = require('fs');

var express = require('express');
var browserify = require('browserify-middleware');
var serve = require('serve-static');

var app = express();

app.set('port', (process.env.PORT || 3000));

app.use('/vendor', serve(path.join(__dirname, '..', 'bower_components')));
app.use('/static', serve(path.join(__dirname, '..', 'static')));

app.get('/index.js', browserify(path.join(__dirname, 'client', 'index.js')));

//app.get('/', function getIndex (req, res) {
//  res.sendFile(path.join(__dirname, 'client', 'index.html'));
//});
app.use(serve(path.join(__dirname, 'client')));

app.use('/rest/v2', require('./api/v2/rest').endpoint);

var cached_themes;
app.get('/rest/themes', function(req, res) {
  if(!cached_themes) {
    var root = 'bower_components/bootswatch';
    cached_themes = fs.readdirSync(root).filter(function(filename) {
      // this is the path we are using in the html file
      return fs.existsSync(path.join(root, filename, 'bootstrap.min.css'));
    });
  }
  res.json(cached_themes);
});


app.listen(app.get('port'), function runServer () {
  console.log('Server running at http://localhost:' + app.get('port'));
});