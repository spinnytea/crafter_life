'use strict';
var path = require('path');

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var nodemon = require('gulp-nodemon');
var lazypipe = require('lazypipe');
var mocha = require('gulp-mocha');
var karma = require('karma');

// WORKAROUND FOR NODEMON BUG - REMOVE WHEN FIXED
// https://github.com/JacksonGariety/gulp-nodemon/issues/33
function exitHandler() { process.exit(0); }
process.once('SIGINT', exitHandler);

var code = { all: [ 'lib/**/*.js' ] };
code.server = code.all.concat([ '!lib/client/**' ]);
code.client = [ 'lib/client/**/*.js' ];

var tests = {
  unit: [ 'spec/unit/**/*.js' ],
  integration: [ 'spec/integration/**/*.js' ],
  client: [ 'spec/client/**/*.js' ]
};

var jshintTasks = lazypipe()
  .pipe(jshint)
  .pipe(jshint.reporter, 'jshint-stylish')
  .pipe(jshint.reporter, 'fail');

gulp.task('lint-client', [], function () {
  return gulp.src(code.client)
    .pipe(jshintTasks());
});

gulp.task('lint-server', [], function () {
  return gulp.src(code.server)
    .pipe(jshintTasks());
});

gulp.task('lint-test', [], function () {
  return gulp.src(
    tests.unit
      .concat(tests.integration)
      .concat(tests.client))
    .pipe(jshintTasks());
});

gulp.task('lint', ['lint-server', 'lint-client', 'lint-test']);

gulp.task('run', ['lint'], function () {
  nodemon({
    script: path.join(__dirname, 'lib', 'server.js'),
    ext: 'js html',
    ignore: ['lib/client/'],
    tasks: ['lint']
  });
});

gulp.task('scratch', ['lint'], function () {
  require('./lib/scratch');
});

gulp.task('integration_test', ['lint'], function() {
  return gulp.src(tests.integration, {read: false})
    .pipe(mocha({reporter: 'nyan', timeout: 4000}));
});
gulp.task('unit_test', ['lint'], function() {
  return gulp.src(tests.unit, {read: false})
    .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('unitd', [], function () {
  gulp.start('unit_test');
  gulp.watch(code.all.concat(tests.unit), [ 'unit_test' ]);
});

gulp.task('initd', [], function () {
  gulp.start('integration_test');
  gulp.watch(code.all.concat(tests.integration), [ 'integration_test' ]);
});

gulp.task('karma', ['lint'], function (done) {
  new karma.Server({
    configFile: path.join(__dirname, 'karma.conf.js')
  }, done).start();
});

gulp.task('default', ['run']);