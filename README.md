# Inspiration

I wanted a tool to help with crafting.

[Crafting as a Service](http://www.ffxivcrafting.com/) Is SO CLOSE, but not quite.
The more I use it, the more I find the features I wanted.
Nevertheless, there are still a few features that it lacks, so I have something to add.
Who knows, maybe I can become a rival.

[Garland Tools](garlandtools.org/db/#) is incredibly useful.

[Fishing](http://ff14angler.com/) is cooooomplicated.
Crafting may not be AS complicated, but we still want something to go to.


# threads to pick up

**Next: unit testing, d3**

* incorporate eightylegs data
    * need: Enemy Location
* ui
    * BUG: craftlist dagItem alignment with different styles
    * BUG: uploading files: sometimes the upload doesn't apply
    * toggle for hide completed items
    * figure out how the game inventory is auto-sorted; then sort the gather list in the same way
    * update the icons
    * export/import Your List
    * "on site" save and load; save progress under a name and recover it; why store it elsewhere
        * import/export: gzip, base64 encode
        * import/export list icons need title text
        * indicate that the data has been loaded (something needs to change; show goal list?)
    * come up with an "optimal route" for each region, use it for sorting regions
    * BUG: recipe list animations: they apply to the buttons, too (try with Paper)
    * BUG: (preventative) the selectedItem and trDagItem use many identical controls; they should be directives so they always act the same
        * find/buy input/checkbox
        * remaining number/check
    * better home page
        * build random adventures? create random progress? show random pieces?
    * better recipe page
    * Code TODOs
    * condensing item min/max levels feels a lot like copy pasta
        * trDagItem Directive
        * recipeSearch Controller
    * colors
        * some buttons blend into the background on some styles
            * e.g. superhero (in a panel! is this okay?) - is this the only one?
            * if this is intended, don't let the border disappear
* review data
    * check to make sure things like "material counts" are actually numbers
    * BUG: Acidic Secretions -> Enchanted Silver Ink (where to get acidic secretions)
* icons are kinda important
    * [jobs](http://www.ffxivcrafting.com/img/jobs/BSM-inactive.png)
    * we have job icons, but we need item icons
* d3 graph
    * for funsies
    * color each "phase" (gather, hunt, buy, pre-craft, craft)
    * render crafting list
        * draw the graph (item icon + some deets)
* find fishing locations
* location mapper
    * params: items + possible locations
    * return: items + possible locations (minimal selection)
    * test with all shops
    * test with selecting location types (gathering vs buying)
* equipment sets?
    * what can be crafted for different classes?
    * these can be precomputed and the results can be tacked onto the items
* can we add links to a database? garland, xivdb, or some wiki? (of course we could link to lodestone)
* Colin feedback
    * United: 100 ether, check something off -> remaing count + check both show for a frame
    * why are there two checkboxes per row: once I'm done, I'm done. I don't care if I bought or gathered it
* fix unit tests
    * integration tests
        * the angular one should list out each feature I want to test
        * basically, if I were to write a unit test procedure to be carried out by hand, what would it look like
        * write them as generic as possible so they don't break often
        * if things move, then the procedures need to change anyway
    * the adventure api should be rock solid
        * the data is MOST IMPORTANT
        * I slipped up once and it undermined my whole perception of the data integrity
        * Colin now questions if the dependency data is correct
        * so TEST IT
* can we compute a crafting order that has the minimum number of class changes while maintaining topological sort?



# v2 (denormalized)

<pre>
item {
  name
  recipe {
    jobs [ { job, level } ]
    materials [ { item, count } ]
    yield
  }
  gather [ {
    job (btn/min)
    name
    level
    area
    coord { x, y}
  } ]
  hunt [ {
    name
    minLevel
    maxLevel
    area
    coord { x, y}
  } ]
  value {
    buy
    sell
  }
  shop [ {
    area
    name
    coord
  } ]
}
</pre>



# Resources

## Final Fantasy

[Mining Locations](http://ffxiv.consolegameswiki.com/wiki/Mining_Node_Locations),
[Botanist Locations](http://ffxiv.consolegameswiki.com/wiki/Botanist_Node_Locations)

[Lodestone](http://na.finalfantasyxiv.com/lodestone/playguide/db/recipe/)

[icons](http://ffxiv.gamerescape.com/wiki/Dictionary_of_Icons)

## WebWeb

[ngAnimate Tutorial](http://www.nganimate.org/)
because angular made animations so <strike>hard to deal with</strike> rich

[Markdown](https://daringfireball.net/projects/markdown/)

... and of course all everything in `package.json` and `bower.json`
