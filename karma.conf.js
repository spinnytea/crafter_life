'use strict';
module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: [ 'bower', 'browserify', 'mocha' ],

    // Add any additional bower packages that should be loaded in the
    // test page. Order matters. Bower module MUST have a "main" field
    bowerPackages: [
      'jquery',
      'angular',
      'angular-route',
      'angular-animate',
      'angular-local-storage'
    ],

    // Options for browserify
    browserify: { debug: true },

    // Test files
    files: [
      'spec/client/**/*.js',
      // Watch the non-test files too, for changes
      { pattern: 'lib/**/*.js', 
        watched: true, included: false, served: false }
    ],

    // Anything to exclude from testing
    exclude: [
      'lib/data_scripts/**/*.js',
      'lib/scratch.js',
      'lib/server.js'
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'spec/client/**/*.js': [ 'browserify' ]
    },

    // If you want real details, change this to 'mocha'
    reporters: ['nyan'],

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
  });
};
