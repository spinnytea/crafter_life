'use strict';
// tests /lib/client/life/adventure XXX should this spec be moved into a similar folder structure?
var expect = require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-things'))
  .expect;
var sinon = require('sinon');
var _ = require('lodash');

var adventure = require('../../lib/client/life/adventure');

describe('adventure', function() {
  it('fetchAllItems', function(done) {
    var cb = sinon.spy(QUERY_TEST_DATA);
    var allItems = {};
    var list = [{lodestoneId: ID_FOR('Bronze Rivets'), count: 1}, {lodestoneId:ID_FOR('Bronze Ingot'), count: 1}];
    adventure.fetchAllItems(allItems, _.pluck(list, 'lodestoneId'), cb).then(function() {

      expect(cb.callCount).to.equal(2);
      expect(cb).to.have.been.calledWith({ lodestoneIds: ['Bronze Rivets', 'Bronze Ingot'].map(ID_FOR) });
      expect(cb).to.have.been.calledWith({ lodestoneIds: ['Copper Ore', 'Tin Ore'].map(ID_FOR) });

      // this is an intermediate step if you don't remove things you've already queried
      expect(cb).to.not.have.been.calledWith({ lodestoneIds: ['Bronze Ingot', 'Copper Ore', 'Tin Ore'].map(ID_FOR) });

      expect(_.keys(allItems)).to.deep.equal(['Bronze Rivets', 'Bronze Ingot', 'Copper Ore', 'Tin Ore'].map(ID_FOR));

    }).then(done.bind(this), done.bind(this));
  });

  it('setupCraftingDAG', function(done) {
    var allItems = {};
    var list = [{lodestoneId: ID_FOR('Bronze Rivets'), count: 1}, {lodestoneId:ID_FOR('Bronze Ingot'), count: 1}];
    adventure.fetchAllItems(allItems, _.pluck(list, 'lodestoneId'), QUERY_TEST_DATA).then(function() {
      var dag = adventure.setupCraftingDAG(allItems, list);

      expect(dag.map).to.be.an('Object');
      expect(dag.list).to.be.an('Array');
      expect(dag.list.length).to.equal(4);

      expect(dag.list).to.deep.equal([
        { id: '3ae8413756e', name: 'Bronze Rivets', order: 1, standard: false, final: true, gatherable: false, minLevel: 2, yield: 1, total: 1, remaining: 1, userInput: { find: 0, buy: 0, isBuy: false } },
        { id: 'ba7b835e608', name: 'Bronze Ingot', order: 2, standard: false, final: true, gatherable: false, minLevel: 1, yield: 1, total: 2, remaining: 2, userInput: { find: 0, buy: 0, isBuy: false } },
        { id: '82a24de366d', name: 'Copper Ore', order: 3, standard: false, final: false, gatherable: true, total: 4, remaining: 4, userInput: { find: 0, buy: 0, isBuy: null } },
        { id: '3662944738e', name: 'Tin Ore', order: 3, standard: false, final: false, gatherable: true, total: 2, remaining: 2, userInput: { find: 0, buy: 0, isBuy: false } },
      ]);

    }).then(done.bind(this), done.bind(this));
  });

  it('buildLocations', function(done) {
    var allItems = {};
    var list = [{lodestoneId: ID_FOR('Bronze Rivets'), count: 1}, {lodestoneId:ID_FOR('Bronze Ingot'), count: 1}];
    adventure.fetchAllItems(allItems, _.pluck(list, 'lodestoneId'), QUERY_TEST_DATA).then(function() {
      var dag = adventure.setupCraftingDAG(allItems, list);
      var locations = adventure.buildLocations(allItems, dag);

      expect(locations).to.be.an('Array');

      // XXX we should probably update the tests?
      // no, form doesn't really matter
      locations = _.reduce(locations, function(ret, loc) { ret[loc.area] = loc.list; return ret; }, {});

      expect(_.keys(locations).length).to.equal(3);
      expect(locations).to.have.property('Central Thanalan');
      expect(locations).to.have.property('Western Thanalan');
      expect(locations).to.have.property('Ul\'dah - Steps of Thal');
      //console.log(locations['Central Thanalan'][0]);
      expect(locations['Central Thanalan']).to.include.something.that.deep.equals({job:{job:'MIN',level:5},item:'Copper Ore',name:'Mineral Deposit',coord:'(x18,y25)'});
      expect(locations['Central Thanalan']).to.include.something.that.deep.equals({job:{job:'MIN',level:10},item:'Tin Ore',name:'Mineral Deposit',coord:'(x20,y22)'});
      expect(locations['Ul\'dah - Steps of Thal']).to.include.something.that.deep.equals({job:{job:'BUY'},item:'Copper Ore',name:'Aistan',coord:'(x10,y13)'});
      // since we are targeting this as something to craft, we don't want to buy it
      expect(locations['Ul\'dah - Steps of Thal']).to.not.include.something.that.deep.equals({job:'BUY',item:'Bronze Ingot',name:'Gigima',coord:'(x13,y13)'});

      dag.map[ID_FOR('Copper Ore')].remaining = 0;
      locations = adventure.buildLocations(allItems, dag);
      locations = _.reduce(locations, function(ret, loc) { ret[loc.area] = loc.list; return ret; }, {});

      expect(_.keys(locations).length).to.equal(2);
      expect(locations).to.have.property('Central Thanalan');
      expect(locations).to.have.property('Western Thanalan');
      expect(locations['Central Thanalan']).to.deep.equal([{job:{job:'MIN',level:10},item:'Tin Ore',name:'Mineral Deposit',coord:'(x20,y22)'}]);

      dag.map[ID_FOR('Tin Ore')].remaining = 0;
      locations = adventure.buildLocations(allItems, dag);
      locations = _.reduce(locations, function(ret, loc) { ret[loc.area] = loc.list; return ret; }, {});

      expect(_.keys(locations)).to.deep.equal([]);
    }).then(done.bind(this), done.bind(this));
  });

  it('recalculateCounts', function(done) {
    var allItems = {};
    var list = [{lodestoneId: ID_FOR('Grilled Trout'), count: 1}];
    adventure.fetchAllItems(allItems, _.pluck(list, 'lodestoneId'), QUERY_TEST_DATA).then(function() {
      var dag;

      function getCounts() {
        return dag.list.reduce(function(counts, dagItem) {
          counts[dagItem.name] = dagItem.total;
          return counts;
        }, {});
      }

      list[0].count = 0;
      dag = adventure.setupCraftingDAG(allItems, list);
      expect(getCounts()).to.deep.equal({ 'Grilled Trout': 0, 'Princess Trout': 0, 'Table Salt': 0, 'Rock Salt': 0, 'Distilled Water': 0, 'Muddy Water': 0 });

      list[0].count = 1;
      dag = adventure.setupCraftingDAG(allItems, list);
      expect(getCounts()).to.deep.equal({ 'Grilled Trout': 1, 'Princess Trout': 1, 'Table Salt': 1, 'Rock Salt': 1, 'Distilled Water': 1, 'Muddy Water': 1 });

      list[0].count = 2;
      dag = adventure.setupCraftingDAG(allItems, list);
      expect(getCounts()).to.deep.equal({ 'Grilled Trout': 2, 'Princess Trout': 2, 'Table Salt': 2, 'Rock Salt': 1, 'Distilled Water': 1, 'Muddy Water': 1 });

      list[0].count = 6;
      dag = adventure.setupCraftingDAG(allItems, list);
      expect(getCounts()).to.deep.equal({ 'Grilled Trout': 6, 'Princess Trout': 6, 'Table Salt': 6, 'Rock Salt': 1, 'Distilled Water': 1, 'Muddy Water': 1 });

      list[0].count = 7;
      dag = adventure.setupCraftingDAG(allItems, list);
      expect(getCounts()).to.deep.equal({ 'Grilled Trout': 7, 'Princess Trout': 7, 'Table Salt': 7, 'Rock Salt': 2, 'Distilled Water': 2, 'Muddy Water': 2 });

    }).then(done.bind(this), done.bind(this));
  });
}); // end adventure

// this was pulled from the database and then cached in hard-coded form
var TEST_DATA = {
  // Bronze Rivets
  // TODO Incorporate Shards (so just pull the full objects for these)
  // TODO clean up coords
  '3ae8413756e': {'lodestoneId':'3ae8413756e','name':'Bronze Rivets','recipe':{'jobs':[{'job':'BSM','level':3},{'job':'ARM','level':2}],'materials':[{'item':'ba7b835e608','count':1}],'yield':1},'_id':'MT8GtPxo2WOXsSqI'},
  'ba7b835e608': {'lodestoneId':'ba7b835e608','name':'Bronze Ingot','recipe':{'jobs':[{'job':'BSM','level':1},{'job':'ARM','level':1}],'materials':[{'item':'82a24de366d','count':2},{'item':'3662944738e','count':1}],'yield':1},'_id':'keDgVdL0b7Dw9LUX','shop':[{'buy':9,'sell':1,'area':'Ul\'dah - Steps of Thal','name':'Gigima','coord':{'x':13,'y':13}}]},
  '82a24de366d': {'lodestoneId':'82a24de366d','name':'Copper Ore','gather':[{'level':5,'job':'MIN','area':'Central Thanalan','name':'Mineral Deposit','coord':{'display':'(x18,y25)'}},{'level':5,'job':'MIN','area':'Western Thanalan','name':'Mineral Deposit','coord':{'x':26,'y':25}}],'_id':'3pDuMEUI7pCRXMOW', 'shop':[{'buy':2,'sell':1,'area':'Ul\'dah - Steps of Thal','name':'Aistan','coord':{'display':'(x10,y13)'}}]},
  '3662944738e': {'lodestoneId':'3662944738e','name':'Tin Ore','gather':[{'level':10,'job':'MIN','area':'Central Thanalan','name':'Mineral Deposit','coord':{'display':'(x20,y22)'}},{'level':10,'job':'MIN','area':'Western Thanalan','name':'Mineral Deposit','coord':{'x':22,'y':28}}],'_id':'fYJ5LH62lh78oXWw'},

  // Grilled Trout
  '2b0bab4841b':{'lodestoneId':'2b0bab4841b','name':'Grilled Trout','recipe':{'jobs':[{'job':'CUL','level':5,'$$hashKey':'object:8674'}],'materials':[{'item':'48ecba7e3bc','count':1},{'item':'f7580421732','count':1}],'yield':1},'_id':'DQVfOhqZWXRPhAKT'},
  '48ecba7e3bc':{'lodestoneId':'48ecba7e3bc','name':'Princess Trout','_id':'QEdFbbQZRAHQ0VgJ'},
  'f7580421732':{'lodestoneId':'f7580421732','name':'Table Salt','recipe':{'jobs':[{'job':'CUL','level':1,'$$hashKey':'object:8659'}],'materials':[{'item':'a7c50cf9297','count':1},{'item':'9e4d79546c7','count':1}],'yield':6},'_id':'aD2AdxZGxwN9DjoR'},
  'a7c50cf9297':{'lodestoneId':'a7c50cf9297','name':'Rock Salt','gather':[{'level':15,'job':'MIN','area':'Central Thanalan','name':'Rocky Outcrop','coord':{'x':14,'y':23}}],'_id':'ZcZU98tvENAj0RXQ'},
  '9e4d79546c7':{'lodestoneId':'9e4d79546c7','name':'Distilled Water','recipe':{'jobs':[{'job':'ALC','level':1,'$$hashKey':'object:8644'}],'materials':[{'item':'0654ae6dda7','count':1}],'yield':1},'_id':'sRPOyzHrW2DIpElg'},
  '0654ae6dda7':{'lodestoneId':'0654ae6dda7','name':'Muddy Water','gather':[{'level':5,'job':'MIN','area':'Central Thanalan','name':'Mineral Deposit','coord':{'x':18,'y':25}},{'level':5,'job':'MIN','area':'Western Thanalan','name':'Mineral Deposit','coord':{'x':26,'y':25}}],'_id':'LgwdafAk57dbc4tR'}
};

function ID_FOR(name) {
  return _.find(TEST_DATA, function(i) { return i.name === name; }).lodestoneId;
}
function QUERY_TEST_DATA(params) {
  return Promise.resolve(params.lodestoneIds.map(function(id) { return TEST_DATA[id]; }));
}
