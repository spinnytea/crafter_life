'use strict';
var expect = require('chai').expect;
var _ = require('lodash');
var Promise = require('bluebird'); // jshint ignore:line
var fs = require('fs');
var readFile = Promise.promisify(fs.readFile);

describe('eightlylegs', function() {
  it('lodestone_npc exists', function() {
    expect(fs.existsSync('./lib/database/eightylegs/lodestone_npc.json')).to.equal(true);
  });

  it('shop prices are always the same', function(done) {
    readFile('./lib/database/eightylegs/lodestone_npc.json', {encoding: 'utf8'}).then(function(file) {
      var itemPrices = {};

      _.values(JSON.parse(file))
        .filter(function(obj) { return obj.type === 'npc'; }) // only npcs
        .filter(function(obj) { return obj.sells; }) // only npcs that sell things
        .forEach(function(obj) {
          _.forEach(obj.sells, function(shop, itemId) {

            if(itemPrices.hasOwnProperty(itemId)) {
              expect(itemPrices[itemId]).to.equal(shop.price);
            } else {
              itemPrices[itemId] = shop.price;
            }

          });
        });

    }).then(done.bind(this), done.bind(this));
  });
}); // end items