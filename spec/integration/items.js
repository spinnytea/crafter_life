'use strict';
var expect = require('chai').expect;

var items = require('../../lib/api/v2/items');

describe('items', function() {
  // deprecated
  // the value of the item moved to the item itself (not in the shop)
  it.skip('shop prices are always the same', function(done) {
    items.query().then(function(list) {
      list = list
        .filter(function(item) { return item.shop && item.shop.length > 1; })
        .filter(function(item) {
          var price = item.shop[0].buy;
          expect(price).to.be.a('Number');

          return item.shop.some(function(s) { return s.buy !== price; });
        });

      expect(list.length).to.equal(0);

    }).then(done.bind(this), done.bind(this));
  });

  describe('query nameTypes', function() {
    it('regex', function(done) {
      items.query({ name: '^initi.*awl', nameType: 'regex' }).then(function(result) {
        expect(result.length).to.equal(1);
        expect(result[0].name).to.equal('Initiate\'s Awl');
      }).then(done.bind(this), done.bind(this));
    });

    it('fuzzy', function(done) {
      items.query({ name: 'Initiate\'s Awl', nameType: 'fuzzy' }).then(function(result) {

        // this is kind of unpredictable
        expect(result.length).to.be.greaterThan(0);
        result.forEach(function(r) {
          expect(r).to.have.property('$rank');
        });

      }).then(done.bind(this), done.bind(this));
    });
  }); // end search
}); // end items